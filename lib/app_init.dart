import 'dart:async';

import 'package:custom_splash/custom_splash.dart';
//import 'package:facebook_audience_network/facebook_audience_network.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/packages.dart' show ScreenUtil;
import 'models/index.dart'
    show
        AppModel,
        BlogModel,
        CartModel,
        CategoryModel,
        FilterAttributeModel,
        FilterTagModel,
        TagModel,
        UserModel;
import 'screens/index.dart' show LoginScreen, OnBoardScreen;
import 'services/index.dart';
import 'widgets/common/animated_splash.dart';
import 'widgets/common/static_splashscreen.dart';

class AppInit extends StatefulWidget {
  final Function onNext;

  AppInit({this.onNext});

  @override
  _AppInitState createState() => _AppInitState();
}

class _AppInitState extends State<AppInit>  {
  final StreamController<bool> _streamInit = StreamController<bool>();

  bool isFirstSeen = false;
  bool isLoggedIn = false;

  Map appConfig;

  /// check if the screen is already seen At the first time
  Future<bool> checkFirstSeen() async {
    /// Ignore if OnBoardOnlyShowFirstTime is set to true.
    if (kAdvanceConfig['OnBoardOnlyShowFirstTime'] == false) {
      return false;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = prefs.getBool('seen') ?? false;
    return _seen;
  }

  /// Check if the App is Login
  Future checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('loggedIn') ?? false;
  }

  Future loadInitData() async {
    try {
      printLog("[AppState] Inital Data");

      isFirstSeen = await checkFirstSeen();
      setState(() {});

      isLoggedIn = await checkLogin();

      /// Load App model config
      Services().setAppConfig(serverConfig);

      Future.delayed(Duration.zero, () async {
        appConfig =
            await Provider.of<AppModel>(context, listen: false).loadAppConfig();
      });
      Future.delayed(Duration.zero, () {
        /// Load more Category/Blog/Attribute Model beforehand
        if (mounted) {
          final lang = Provider.of<AppModel>(context, listen: false).langCode;

          Provider.of<CategoryModel>(context, listen: false).getCategories(
            lang: lang,
            sortingList:
                Provider.of<AppModel>(context, listen: false).categories,
          );

          Provider.of<TagModel>(context, listen: false).getTags();

//          Provider.of<BlogModel>(context, listen: false).getBlogs();

          Provider.of<FilterTagModel>(context, listen: false).getFilterTags();

          Provider.of<FilterAttributeModel>(context, listen: false)
              .getFilterAttributes();

          Provider.of<CartModel>(context, listen: false).changeCurrencyRates(
              Provider.of<AppModel>(context, listen: false).currencyRate);

          //save logged in user
          Provider.of<CartModel>(context, listen: false)
              .setUser(Provider.of<UserModel>(context, listen: false).user);
          if (isLoggedIn) {
            /// Preload address.
            Provider.of<CartModel>(context, listen: false).getAddress();
          }
        }
      });

      /// Firebase Dynamic Link Init
      if (firebaseDynamicLinkConfig['isEnabled']) {
        printLog("[dynamic_link] Firebase Dynamic Link Init");
        DynamicLinkService dynamicLinkService = DynamicLinkService();
        dynamicLinkService.generateFirebaseDynamicLink();
      }

      /// Facebook Ads init
      if (kAdConfig['enable']) {
        debugPrint("[AppState] Init Facebook Audience Network");
//        await FacebookAudienceNetwork.init();
      }

      printLog("[AppState] Init Data Finish");
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
    setState(() {});
  }

  Widget onNextScreen(bool isFirstSeen) {
    if (!isFirstSeen && !kIsWeb && appConfig != null) {
      if (onBoardingData.isNotEmpty) return OnBoardScreen(appConfig);
    }

    if (kLoginSetting['IsRequiredLogin'] && !isLoggedIn) {
      return LoginScreen(
        onLoginSuccess: (context) async {
          await Navigator.pushNamed(context, RouteList.dashboard);
        },
      );
    }
    return widget.onNext();
  }

  @override
  void initState() {
    loadInitData();

    super.initState();
  }

  @override
  void dispose() {
    _streamInit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String splashScreenType = kSplashScreenType;
    dynamic splashScreenData = kSplashScreen;

    if (appConfig != null && appConfig['SplashScreen'] != null) {
      splashScreenType = appConfig['SplashScreen']['type'];
      splashScreenData = appConfig['SplashScreen']['data'];
    }

    if (splashScreenType == 'flare') {
      return SplashScreen.navigate(
        name: splashScreenData,
        startAnimation: 'fluxstore',
        backgroundColor: Colors.white,
        next: (object) => onNextScreen(isFirstSeen),
        until: () => Future.delayed(const Duration(seconds: 6)),
      );
    }

    if (splashScreenType == 'animated') {
      debugPrint('[FLARESCREEN] Animated');
      return
        AnimatedSplash(
        imagePath: splashScreenData,
        home: onNextScreen(isFirstSeen),
        duration: 6500,
        type: AnimatedSplashType.StaticDuration,
        isPushNext: true,
      );
    }
    if (splashScreenType == 'zoomIn') {
      return CustomSplash(
        imagePath: splashScreenData,
        backGroundColor: Colors.white,
        animationEffect: 'zoom-in',
        logoSize: 50,
        home: onNextScreen(isFirstSeen),
        duration: 6500,
      );
    }
    if (splashScreenType == 'static') {
      return StaticSplashScreen(
        imagePath: splashScreenData,
        onNextScreen: onNextScreen(isFirstSeen),
      );
    }
    return Container();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    ScreenUtil.init(context);
  }

}
