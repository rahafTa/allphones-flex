import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'generated/l10n.dart';
import 'models/app_model.dart';
import 'models/category_model.dart';
import 'models/entities/category.dart';
import 'models/product_model.dart';
import 'models/user_model.dart';
import 'tabbar.dart';
import 'widgets/layout/adaptive.dart';

class MenuBar extends StatefulWidget {
  MenuBar();

  @override
  _MenuBarState createState() => _MenuBarState();
}

class _MenuBarState extends State<MenuBar> {
  TextStyle ts = TextStyle(fontFamily: 'DroidKuffi');

  void pushNavigation(String name) {
    eventBus.fire(const EventCloseNativeDrawer());
    return MainTabControlDelegate.getInstance()
        .changeTab(name.replaceFirst('/', ''));
  }

  @override
  Widget build(BuildContext context) {
    printLog("[MenuBar] build");
    Map<String, dynamic> drawer =
        Provider.of<AppModel>(context, listen: false).drawer ?? kDefaultDrawer;

    return Column(
      key: drawer['key'] != null ? Key(drawer['key']) : null,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (drawer['logo'] != null)
                    Center(child: Container(

                      height: 150,
                      width: 150,
                      child: imageContainer(drawer['logo']),
                    )),

                  const Divider(),
                  ...List.generate(drawer['items'].length, (index) {
                    return drawerItem(drawer['items'][index]);
                  }),
                  isDisplayDesktop(context)
                      ? const SizedBox(height: 300)
                      : const SizedBox(height: 24),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget imageContainer(String link) {
    if (link.contains('http://') || link.contains('https://')) {
      return Image.network(
        link,
//        fit: BoxFit.cover,
        height: 45,
        width: 45,
      );
    }
    return Image.asset(
      link,
//      fit: BoxFit.cover,
      height: 45,
      width: 45,
    );
  }

  Widget drawerItem(item) {
    // final isTablet = Tools.isTablet(MediaQuery.of(context));



    if (item['show'] == false) return Container();
    switch (item['type']) {
      case 'home':
        {
          return ListTile(
            leading: const Icon(
              Icons.shopping_basket,
              size: 20,
            ),
            title: Text(S.of(context).shop,style: ts,),
            onTap: () {
              pushNavigation(RouteList.home);
            },
          );
        }
      case 'categories':
        {
          return ListTile(
            leading: const Icon(Icons.category, size: 20),
            title: Text(S.of(context).categories,style: ts,),
            onTap: () => pushNavigation(
                Provider.of<AppModel>(context, listen: false).vendorType ==
                        VendorType.single
                    ? RouteList.category
                    : RouteList.vendorCategory),
          );
        }
      case 'cart':
        {
          return ListTile(
            leading: const Icon(Icons.shopping_cart, size: 20),
            title: Text(S.of(context).cart,style: ts,),
            onTap: () => pushNavigation(RouteList.cart),
          );
        }
      case 'profile':
        {
          return ListTile(
            leading: const Icon(Icons.person, size: 20),
            title: Text(S.of(context).settings,style: ts,),
            onTap: () => pushNavigation(RouteList.profile),
          );
        }
      case 'web':
        {
          return kIsWeb || isDisplayDesktop(context)
              ? Column(
                  children: [
                    ListTile(
                      leading: const Icon(
                        Icons.list,
                        size: 20,
                      ),
                      title: Text(S.of(context).category,style: ts,),
                      onTap: () {
                        pushNavigation(RouteList.category);
                      },
                    ),
                    ListTile(
                      leading: const Icon(
                        Icons.search,
                        size: 20,
                      ),
                      title: Text(S.of(context).search,style: ts,),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: const Icon(Icons.settings, size: 20),
                      title: Text(S.of(context).settings,style: ts,),
                      onTap: () {
                        if (kIsWeb) {
                        } else {
                          Navigator.of(context).pushNamed(RouteList.profile);
                        }
                      },
                    )
                  ],
                )
              : const SizedBox();
        }

      case 'login':
        {
          return ListenableProvider.value(
            value: Provider.of<UserModel>(context, listen: false),
            child: Consumer<UserModel>(builder: (context, userModel, _) {
              final loggedIn = userModel.loggedIn;
              return ListTile(
                leading: const Icon(Icons.exit_to_app, size: 20),
                title: loggedIn
                    ? Text(S.of(context).logout,style: ts,)
                    : Text(S.of(context).login,style: ts,),
                onTap: () {
                  if (loggedIn) {
                    Provider.of<UserModel>(context, listen: false).logout();
                    if (kLoginSetting['IsRequiredLogin'] ?? false) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                        RouteList.login,
                        (route) => false,
                      );
                    } else {
                      pushNavigation(RouteList.dashboard);
                    }
                  } else {
                    pushNavigation(RouteList.login);
                  }
                },
              );
            }),
          );
        }
      case 'category':
        {
          return buildListCategory();
        }
//        case 'rate':
//        {
//       var con =   kAdvanceConfig['Currencies'] as List;
//          return Container(child: ListTile(
//              leading: Icon(Icons.monetization_on),
//              title: Text(S.of(context).currencyRate +' = ' +con.first['smallestUnitRate'].toString()+' ' +S.of(context).currencyIQD
//              )),);
//        }
      default:
        return Container();
    }
  }

  Widget buildListCategory() {
    final categories = Provider.of<CategoryModel>(context).categories;
    List<Widget> widgets = [];

    if (categories != null) {
      var list = categories.where((item) => item.parent == '0').toList();
      for (var i = 0; i < list.length; i++) {
        final currentCategory = list[i];
        var childCategories =
            categories.where((o) => o.parent == currentCategory.id).toList();
        widgets.add(Container(
          color: i.isOdd
              ? Theme.of(context).backgroundColor
              : Theme.of(context).primaryColorLight,

          /// Check to add only parent link category
          child: childCategories.isEmpty
              ? InkWell(
                  onTap: () {
                    ProductModel.showList(
                      context: context,
                      cateId: currentCategory.id,
                      cateName: currentCategory.name,
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: 20,
                      bottom: 12,
                      left: 16,
                      top: 12,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: Text(currentCategory.name.toUpperCase(),style: ts,)),
                        const SizedBox(width: 24),
                        currentCategory.totalProduct == null
                            ? const Icon(Icons.chevron_right)
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  S
                                      .of(context)
                                      .nItems(currentCategory.totalProduct),
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 12,
                                    fontFamily: 'DroidKuffi'
                                  ),
                                ),
                              ),
                      ],
                    ),
                  ),
                )
              : ExpansionTile(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 0),
                    child: Text(
                      currentCategory.name.toUpperCase(),
                      style: const TextStyle(fontSize: 14,fontFamily: 'DroidKuffi'),
                    ),
                  ),
                  children:
                      getChildren(categories, currentCategory, childCategories),
                ),
        ));
      }
    }

    return ExpansionTile(
      initiallyExpanded: false,
      expandedCrossAxisAlignment: CrossAxisAlignment.start,
      tilePadding: const EdgeInsets.only(left: 16, right: 8),
      title: Text(
        S.of(context).byCategory.toUpperCase(),
        style: TextStyle(
          fontFamily: 'DroidKuffi',
          fontSize: 14,
          fontWeight: FontWeight.w600,
          color: Theme.of(context).accentColor.withOpacity(0.5),
        ),
      ),
      children: widgets,
    );
  }

  List getChildren(List<Category> categories, Category currentCategory,
      List<Category> childCategories) {
    List<Widget> list = [];

    list.add(
      ListTile(
        leading: Padding(
          child: Text(S.of(context).seeAll),
          padding: const EdgeInsets.only(left: 20),
        ),
        trailing: Text(
          S.of(context).nItems(currentCategory.totalProduct),
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 12,
            fontFamily: 'DroidKuffi'
          ),
        ),
        onTap: () {
          ProductModel.showList(
            context: context,
            cateId: currentCategory.id,
            cateName: currentCategory.name,
          );
        },
      ),
    );
    for (var i in childCategories) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(i.name),
            padding: const EdgeInsets.only(left: 20),
          ),
          trailing: Text(
            S.of(context).nItems(i.totalProduct),
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 12,
              fontFamily: 'DroidKuffi'
            ),
          ),
          onTap: () {
            ProductModel.showList(
                context: context, cateId: i.id, cateName: i.name);
          },
        ),
      );
    }
    return list;
  }
}
