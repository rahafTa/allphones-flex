import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/config.dart';
import '../common/constants.dart';
import '../services/index.dart';
import 'cart/cart_model.dart';
import 'category_model.dart';
import 'entities/curriency.dart';
class AppModel with ChangeNotifier {
  Map<String, dynamic> appConfig;
  bool isLoading = true;
  String message;
  bool darkTheme = kDefaultDarkTheme ?? false;
  String _langCode = kAdvanceConfig['DefaultLanguage'];
  List<String> categories;
  String productListLayout;
  List<int> ratioProduct;
  String currency; //USD, VND
  String currencyCode;
  int smallestUnitRate;
  Map<String, dynamic> currencyRate = Map<String, dynamic>();
  bool showDemo = false;
  String username;
  bool isInit = false;
  Map<String, dynamic> drawer;
  Map deeplink;
  VendorType vendorType;

  String get langCode => _langCode;

  AppModel() {
    getConfig();
    vendorType = kFluxStoreMV.contains(serverConfig['type'])
        ? VendorType.multi
        : VendorType.single;
  }

  Future<bool> getConfig() async {
    print('connnfiiiig');
         // String va =  await   getCurrentValue();
    try {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      var defaultCurrency = kAdvanceConfig['DefaultCurrency'] as Map;

      _langCode =
          prefs.getString("language") ?? kAdvanceConfig['DefaultLanguage'];
      darkTheme = prefs.getBool("darkTheme") ?? false;
      currency = prefs.getString("currency") ?? defaultCurrency['currency'];
      currencyCode =
          prefs.getString("currencyCode") ?? defaultCurrency['currencyCode'];
      smallestUnitRate = defaultCurrency['smallestUnitRate'];
      // smallestUnitRate =6500;
      isInit = true;

      await loadAppConfig();
      return true;
    } catch (err) {
      return false;
    }
  }
static  Future<String> getCurrentValue() async {
    String value;
    try {
      print('innnnnn Currennnn');
      var response = await http.get(
          "https://ahmad.somarmeat.com/public/currencys/9");
      print(response.toString());
      print('---------------------------');
      CurrencyIqd currencyIqdJson;
      if (response.statusCode == 200) {
        currencyIqdJson = CurrencyIqd.fromJson(convert.jsonDecode(response.body));
      }

      return currencyIqdJson.currencyValue;

    } catch (e) {
      print('innnnnn Currennnn errrrrrroor');
      print(e);
      rethrow;
    }
  }

  Future<bool> changeLanguage(String country, BuildContext context) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      _langCode = country;
      await prefs.setString("language", _langCode);
      await loadAppConfig(isSwitched: true);
      eventBus.fire(const EventChangeLanguage());

      await Provider.of<CategoryModel>(context, listen: false)
          .getCategories(lang: country, sortingList: categories);

      return true;
    } catch (err) {
      return false;
    }
  }

  Future<void> changeCurrency(String item, BuildContext context) async {
    try {
      Provider.of<CartModel>(context, listen: false).changeCurrency(item);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      currency = item;
      await prefs.setString("currency", currency);
      notifyListeners();
    } catch (error) {
      printLog('[_getFacebookLink] error: ${error.toString()}');
    }
  }

  Future<void> updateTheme(bool theme) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      darkTheme = theme;
      await prefs.setBool("darkTheme", theme);
      notifyListeners();
    } catch (error) {
      printLog('[_getFacebookLink] error: ${error.toString()}');
    }
  }

  void updateShowDemo(bool value) {
    showDemo = value;
    notifyListeners();
  }

  void updateUsername(String user) {
    username = user;
    notifyListeners();
  }

  void loadStreamConfig(config) {
    appConfig = config;
    productListLayout = appConfig['Setting']['ProductListLayout'];
    isLoading = false;
    notifyListeners();
  }

  Future<Map> loadAppConfig({isSwitched = false}) async {
    // String va =  await   getCurrentValue();
    try {
      if (!isInit) {
        await getConfig();
      }
      final LocalStorage storage = LocalStorage('builder.json');
      var config = await storage.getItem('config');
      if (config != null) {
        appConfig = config;
      } else {
        /// we only apply the http config if isUpdated = false, not using switching language
        // ignore: prefer_contains
        if (kAppConfig.indexOf('http') != -1) {
          // load on cloud config and update on air
          String path = kAppConfig;
          if (path.contains('.json')) {
            path = path.substring(0, path.lastIndexOf('/'));
            path += '/config_$langCode.json';
          }
          final appJson = await http.get(Uri.encodeFull(path),
              headers: {"Accept": "application/json"});
          appConfig =
              convert.jsonDecode(convert.utf8.decode(appJson.bodyBytes));
        } else {
          // load local config
          String path = "lib/config/config_$langCode.json";
          try {
            final appJson = await rootBundle.loadString(path);
            appConfig = convert.jsonDecode(appJson);
          } catch (e) {
            final appJson = await rootBundle.loadString(kAppConfig);
            appConfig = convert.jsonDecode(appJson);
          }
        }
      }

      /// Load Product ratio from config file
      productListLayout = appConfig['Setting']['ProductListLayout'];
      final ratioImageProduct = appConfig['Setting']['ratioImageProduct'];
      final ratioInfoProduct = appConfig['Setting']['ratioInfoProduct'];
      if (ratioImageProduct != null && ratioInfoProduct != null) {
        ratioProduct = [ratioImageProduct, ratioInfoProduct];
      }

      drawer = appConfig['Drawer'] != null
          ? Map<String, dynamic>.from(appConfig['Drawer'])
          : null;

      /// Load categories config for the Tabbar menu
      /// User to sort the category Setting
      var categoryTab = appConfig['TabBar']
          .firstWhere((e) => e['layout'] == 'category', orElse: () => {});
      if (categoryTab['categories'] != null) {
        categories = List<String>.from(categoryTab['categories']);
      }

      /// apply App Caching if isCaching is enable
      await Services().widget.onLoadedAppConfig(langCode, (configCache) {
        appConfig = configCache;
      });

      /// Load the Rate for Product Currency
      final rates = await Services().getCurrencyRate();
      if (rates != null) {
        print(rates);
        print('raatttte');
        currencyRate = rates;
      }
      isLoading = false;

      printLog('[Debug] Finish Load AppConfig');

      notifyListeners();

      return appConfig;
    } catch (err, trace) {
      printLog(trace);
      isLoading = false;
      message = err.toString();
      notifyListeners();
      return null;
    }
  }

  void updateProductListLayout(layout) {
    productListLayout = layout;
    notifyListeners();
  }
   notifiForchange(){
    notifyListeners();
  }
}
