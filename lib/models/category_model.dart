import 'package:flutter/material.dart';

import '../common/constants.dart';
import '../common/tools.dart';
import '../services/index.dart';
import 'entities/category.dart';

class CategoryModel with ChangeNotifier {
  final Services _service = Services();
  List<Category> categories;
  Map<String, Category> categoryList = {};

  bool isLoading = false;
  String message;

  /// Format the Category List and assign the List by Category ID
  static Map<String, Category> sortCategoryList(
      {List<Category> categoryList, List<Category> sortingList}) {
    Map<String, Category> _categoryList = {};
    List<Category> result = categoryList;

    if (sortingList != null) {
      List<Category> _categories = [];
      List<Category> _subCategories = [];
      bool isParent = true;
      for (var category in sortingList) {
        Category item = categoryList.firstWhere(
            (Category cat) => cat.id.toString() == category.toString(),
            orElse: () => null);
        if (item != null) {
          if (item.parent != '0') {
            isParent = false;
          }
          _categories.add(item);
        }
      }

      for (var category in categoryList) {
        Category item = _categories.firstWhere((cat) => cat.id == category.id,
            orElse: () => null);
        if (item == null && isParent && category.parent != '0') {
          _subCategories.add(category);
        }
      }
      result = [..._categories, ..._subCategories];
    }

    for (Category cat in result) {
      _categoryList[cat.id] = cat;
    }

    return _categoryList;
  }

  Future<void> getCategories({lang, sortingList}) async {
    try {
      printLog("[Category] getCategories");
      isLoading = true;
      notifyListeners();
      categories = await _service.getCategories(lang: lang);
      message = null;

      ///----FLUXSTORE LISTING----///
      if (Utils.isListingTheme()) {
        for (Category cat in categories) {
          categoryList[cat.id] = cat;
        }
        isLoading = false;
        notifyListeners();

        ///----FLUXSTORE LISTING----///
      } else {
        categoryList = sortCategoryList(
            categoryList: categories, sortingList: sortingList);
        isLoading = false;
        notifyListeners();
      }
    } catch (err, _) {
      isLoading = false;
      message = "There is an issue with the app during request the data, "
              "please contact admin for fixing the issues " +
          err.toString();
      notifyListeners();
    }
  }
}
