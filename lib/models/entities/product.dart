import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:quiver/strings.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../services/service_config.dart';
import '../serializers/product.dart';
import '../vendor/store_model.dart';
import 'menu_price.dart';
import 'product_attribute.dart';
import 'product_variation.dart';

class Product {
  String id;
  String sku;
  String name;
  String vendor;
  String description;
  String permalink;
  String price;
  String regularPrice;
  String salePrice;
  bool onSale;
  bool inStock;
  double averageRating;
  int ratingCount;
  List<String> images;
  String imageFeature;
  List<ProductAttribute> attributes;
  Map<String, String> attributeSlugMap = {};
  List<Attribute> defaultAttributes;
  List<ProductAttribute> infors = [];
  String categoryId;
  String videoUrl;
  List<dynamic> groupedProducts;
  List<String> files;
  int stockQuantity;
  int minQuantity;
  int maxQuantity;
  bool manageStock;
  bool backOrdered = false;
  Store store;

  /// is to check the type affiliate, simple, variant
  String type;
  String affiliateUrl;
  List<ProductVariation> variations;

  List<Map<String, dynamic>> options; //for opencart

  String idShop; //for prestashop

  ///----FLUXSTORE LISTING----///

  String distance;
  Map pureTaxonomies;
  List reviews;
  String featured;
  String verified;
  String tagLine;
  String priceRange;
  String categoryName;
  String hours;
  String location;
  String phone;
  String facebook;
  String email;
  String website;
  String skype;
  String whatsapp;
  String youtube;
  String twitter;
  String instagram;
  String eventDate;
  String rating;
  int totalReview = 0;
  double lat;
  double long;
  List<dynamic> prices = [];

  ///----FLUXSTORE LISTING----///

  Product.empty(this.id) {
    name = '';
    price = '0.0';
    imageFeature = '';
  }

  bool isEmptyProduct() {
    return name == '' && price == '0.0' && imageFeature == '';
  }

  Product.copyWith(Product p) {
    id = p.id;
    sku = p.sku;
    name = p.name;
    description = p.description;
    permalink = p.permalink;
    price = p.price;
    regularPrice = p.regularPrice;
    salePrice = p.salePrice;
    onSale = p.onSale;
    inStock = p.inStock;
    averageRating = p.averageRating;
    ratingCount = p.ratingCount;
    images = p.images;
    imageFeature = p.imageFeature;
    attributes = p.attributes;
    infors = p.infors;
    categoryId = p.categoryId;
    videoUrl = p.videoUrl;
    groupedProducts = p.groupedProducts;
    files = p.files;
    stockQuantity = p.stockQuantity;
    minQuantity = p.minQuantity;
    maxQuantity = p.maxQuantity;
    manageStock = p.manageStock;
    backOrdered = p.backOrdered;
    type = p.type;
    affiliateUrl = p.affiliateUrl;
    variations = p.variations;
    options = p.options;
    idShop = p.idShop;
  }

  Product.fromJson(Map<String, dynamic> parsedJson) {
    try {
      id = parsedJson["id"].toString();
      sku = parsedJson['sku'];

      name = parsedJson["name"];
      type = parsedJson["type"];
      description = isNotBlank(parsedJson["description"])
          ? parsedJson["description"]
          : parsedJson["short_description"];
      permalink = parsedJson["permalink"];
      price = parsedJson["price"] != null ? parsedJson["price"].toString() : "";

      regularPrice = parsedJson["regular_price"] != null
          ? parsedJson["regular_price"].toString()
          : null;
      salePrice = parsedJson["sale_price"] != null
          ? parsedJson["sale_price"].toString()
          : null;
      onSale = parsedJson["price"] != parsedJson["regular_price"] &&
          isNotBlank(parsedJson["regular_price"]) &&
          double.parse(parsedJson["regular_price"]) >
              double.parse(parsedJson["price"]);
      inStock =
          parsedJson["in_stock"] ?? parsedJson["stock_status"] == "instock";
      backOrdered = parsedJson["backordered"] ?? false;

      averageRating = double.parse(parsedJson["average_rating"] ?? "0.0");
      ratingCount = int.parse((parsedJson["rating_count"] ?? 0).toString());
      categoryId = parsedJson["categories"] != null &&
              parsedJson["categories"].length > 0
          ? parsedJson["categories"][0]["id"].toString()
          : '0';

      manageStock = parsedJson['manage_stock'] ?? false;

      // add stock limit
      if (parsedJson['manage_stock'] == true) {
        stockQuantity = parsedJson['stock_quantity'];
      }

      //minQuantity = parsedJson['meta_data']['']

      List<ProductAttribute> attributeList = [];
      parsedJson["attributesData"]?.forEach((item) {
        if (item['visible'] && item['variation']) {
          final ProductAttribute attr = ProductAttribute.fromJson(item);
          attributeList.add(attr);

          for (var option in attr?.options) {
            attributeSlugMap[option['slug']] = option['name'];
          }
        }
      });
      attributes = attributeList.toList();

      List<Attribute> _defaultAttributes = [];
      parsedJson["default_attributes"]?.forEach((item) {
        _defaultAttributes.add(Attribute.fromJson(item));
      });
      defaultAttributes = _defaultAttributes.toList();

      parsedJson["attributes"]?.forEach((item) {
        if (item['visible'] ?? true) {
          infors.add(ProductAttribute.fromLocalJson(item));
        }
      });

      List<String> list = [];
      if (parsedJson["images"] != null) {
        for (var item in parsedJson["images"]) {
          list.add(item["src"]);
        }
      }

      images = list;
      imageFeature = images.isNotEmpty ? images[0] : null;

      /// get video links, support following plugins
      /// - WooFeature Video: https://wordpress.org/plugins/woo-featured-video/
      ///- Yith Feature Video: https://wordpress.org/plugins/yith-woocommerce-featured-video/
      var video = parsedJson['meta_data'].firstWhere(
        (item) =>
            item['key'] == '_video_url' || item['key'] == '_woofv_video_embed',
        orElse: () => null,
      );
      if (video != null) {
        videoUrl = video['value'] is String
            ? video['value']
            : video['value']['url'] ?? '';
      }

      affiliateUrl = parsedJson['external_url'];

      List<int> groupedProductList = [];
      parsedJson['grouped_products']?.forEach((item) {
        groupedProductList.add(item);
      });
      groupedProducts = groupedProductList;
      List<String> files = [];
      parsedJson['downloads']?.forEach((item) {
        files.add(item['file']);
      });
      this.files = files;

      if (parsedJson['meta_data'] != null) {
        for (var item in parsedJson['meta_data']) {
          try {
            if (item['key'] == '_minmax_product_max_quantity') {
              int quantity = int.parse(item['value']);
              quantity == 0 ? maxQuantity = null : maxQuantity = quantity;
            }
          } catch (e) {
            printLog('maxQuantity $e');
          }

          try {
            if (item['key'] == '_minmax_product_min_quantity') {
              int quantity = int.parse(item['value']);
              quantity == 0 ? minQuantity = null : minQuantity = quantity;
            }
          } catch (e) {
            printLog('minQuantity $e');
          }
        }
      }
    } catch (e, trace) {
      printLog(trace);
      printLog(e.toString());
    }
  }

  Product.fromOpencartJson(Map<String, dynamic> parsedJson) {
    try {
      id = parsedJson["product_id"] != null ? parsedJson["product_id"] : '0';
      name = HtmlUnescape().convert(parsedJson["name"]);
      description = parsedJson["description"];
      permalink = parsedJson["permalink"];
      regularPrice = parsedJson["price"];
      salePrice = parsedJson["special"];
      price = salePrice ?? regularPrice;
      onSale = salePrice != null;
      inStock = parsedJson["stock_status"] == "In Stock" ||
          int.parse(parsedJson["quantity"]) > 0;
      averageRating = parsedJson["rating"] != null
          ? double.parse(parsedJson["rating"].toString())
          : 0.0;
      ratingCount = parsedJson["reviews"] != null
          ? int.parse(parsedJson["reviews"].toString())
          : 0.0;
      attributes = [];

      List<String> list = [];
      if (parsedJson["images"] != null && parsedJson["images"].length > 0) {
        for (var item in parsedJson["images"]) {
          list.add(item);
        }
      }
      if (list.isEmpty && parsedJson['image'] != null) {
        list.add('${Config().url}/image/${parsedJson['image']}');
      }
      images = list;
      imageFeature = images.isNotEmpty ? images[0] : "";
      options = List<Map<String, dynamic>>.from(parsedJson['options']);
    } catch (e) {
      debugPrintStack();
      printLog(e.toString());
    }
  }

  Product.fromMagentoJson(Map<String, dynamic> parsedJson) {
    try {
      id = "${parsedJson["id"]}";
      sku = parsedJson["sku"];
      name = parsedJson["name"];
      permalink = parsedJson["permalink"];
      inStock = parsedJson["status"] == 1;
      averageRating = 0.0;
      ratingCount = 0;
      categoryId = "${parsedJson["category_id"]}";
      attributes = [];
    } catch (e) {
      debugPrintStack();
      printLog(e.toString());
    }
  }

  Product.fromShopify(Map<String, dynamic> json) {
    try {
      var priceV2 = json['variants']['edges'][0]['node']['priceV2'];
      var compareAtPriceV2 =
          json['variants']['edges'][0]['node']['compareAtPriceV2'];
      var compareAtPrice =
          compareAtPriceV2 != null ? compareAtPriceV2['amount'] : null;
      var categories =
          json['collections'] != null ? json['collections']['edges'] : null;
      var defaultCategory = categories != null ? categories[0]['node'] : null;

      categoryId = json['categoryId'] ?? defaultCategory['id'];
      id = json['id'];
      sku = json['sku'];
      name = json['title'];
      vendor = json['vendor'];
      description = json['description'];
      price = priceV2 != null ? priceV2['amount'] : null;
      regularPrice = compareAtPrice ?? price;
      onSale = compareAtPrice != null && compareAtPrice != price;
      inStock = json['availableForSale'];
      ratingCount = 0;
      averageRating = 0;
      permalink = json['onlineStoreUrl'];

      List<String> imgs = [];

      if (json['images']['edges'] != null) {
        for (var item in json['images']['edges']) {
          imgs.add(item['node']['src']);
        }
      }

      images = imgs;
      imageFeature = images[0];

      List<ProductAttribute> attrs = [];

      if (json['options'] != null) {
        for (var item in json['options']) {
          attrs.add(ProductAttribute.fromShopify(item));
        }
      }

      attributes = attrs;
      List<ProductVariation> variants = [];

      if (json['variants']['edges'] != null) {
        for (var item in json['variants']['edges']) {
          variants.add(ProductVariation.fromShopifyJson(item['node']));
        }
      }

      variations = variants;
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
  }

  Product.fromPresta(Map<String, dynamic> parsedJson, apiLink) {
    try {
      id = parsedJson["id"] != null ? parsedJson["id"].toString() : '0';
      name = parsedJson["name"];
      description =
          parsedJson["description"] is String ? parsedJson["description"] : '';
      permalink = parsedJson["link_rewrite"];
      regularPrice = (double.parse((parsedJson["price"] ?? 0.0).toString()))
          .toStringAsFixed(2);
      salePrice =
          (double.parse((parsedJson["wholesale_price"] ?? 0.0).toString()))
              .toStringAsFixed(2);
      price = (double.parse((parsedJson["wholesale_price"] ?? 0.0).toString()))
          .toStringAsFixed(2);
      idShop = parsedJson["id_shop_default"] != null
          ? parsedJson["id_shop_default"].toString()
          : null;
      ratingCount = 0;
      averageRating = 0.0;
      if (salePrice != regularPrice) {
        onSale = true;
      } else {
        onSale = false;
      }
      imageFeature = parsedJson["id_default_image"] != null
          ? apiLink('images/products/$id/${parsedJson["id_default_image"]}')
          : null;
      images = [];
      if (parsedJson["associations"] != null &&
          parsedJson["associations"]["images"] != null) {
        for (var item in parsedJson["associations"]["images"]) {
          images.add(apiLink('images/products/$id/${item["id"]}'));
        }
      } else {
        images.add(imageFeature);
      }
      if (parsedJson["associations"] != null &&
          parsedJson["associations"]["stock_availables"] != null) {
        sku = parsedJson["associations"]["stock_availables"][0]["id"];
      }
      type = parsedJson['type'];
      if (parsedJson['quantity'] != null &&
          parsedJson['quantity'].toString().isNotEmpty) {
        stockQuantity = int.parse(parsedJson['quantity']);
        if (stockQuantity > 0) inStock = true;
      }
      if (inStock == null) inStock = false;
      if (parsedJson["associations"] != null &&
          parsedJson["associations"]["product_bundle"] != null) {
        groupedProducts = parsedJson["associations"]["product_bundle"];
      }
      List<ProductAttribute> attrs = [];
      if (parsedJson['attributes'] != null) {
        var res = Map<String, dynamic>.from(parsedJson['attributes']);
        var keys = res.keys.toList();
        for (var i = 0; i < keys.length; i++) {
          attrs.add(ProductAttribute.fromPresta(
              {'id': i, 'name': keys[i], 'options': res[keys[i]]}));
        }
        attributes = attrs;
      } else {
        attributes = [];
      }
    } catch (e, trace) {
      printLog(trace);
      printLog(e.toString());
    }
  }

  Product.fromJsonStrapi(SerializerProduct model, apiLink) {
    try {
      id = model.id.toString();
      name = model.title;
      inStock = !model.isOutOfStock;
      stockQuantity = model.inventory;
      images = [];
      if (model.images != null) {
        for (var item in model.images) {
          images.add(apiLink(item.url));
        }
      }
      imageFeature =
          images.isNotEmpty ? images[0] : apiLink(model.thumbnail.url);

      ratingCount = model.review;
      averageRating = model.review.toDouble();
      price = model.price.toString();
      regularPrice = model.price.toString();
      salePrice = model.salePrice.toString();

      if (model.productCategories != null) {
        categoryId = model.productCategories.isNotEmpty
            ? model.productCategories[0].id.toString()
            : '0';
      } else {
        categoryId = '0';
      }
      onSale = model.isSale;
    } catch (e, trace) {
      printLog(e);
      printLog(trace);
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "sku": sku,
      "name": name,
      "description": description,
      "permalink": permalink,
      "price": price,
      "regularPrice": regularPrice,
      "salePrice": salePrice,
      "onSale": onSale,
      "inStock": inStock,
      "averageRating": averageRating,
      "ratingCount": ratingCount,
      "images": images,
      "imageFeature": imageFeature,
      "attributes": attributes?.map((e) => e.toJson()),
      "categoryId": categoryId,
      "stock_quantity": stockQuantity,
      "idShop": idShop,
      "store": store?.toJson(),

      ///----FluxStore Listing----///
      'distance': distance,
      'pureTaxonomies': pureTaxonomies,
      'reviews': reviews,
      'featured': featured,
      'verified': verified,
      'tagLine': tagLine,
      'priceRange': priceRange,
      'categoryName': categoryName,
      'hours': hours,
      'location': location,
      'phone': phone,
      'facebook': facebook,
      'email': email,
      'website': website,
      'skype': skype,
      'whatsapp': whatsapp,
      'youtube': youtube,
      'twitter': twitter,
      'instagram': instagram,
      'eventDate': eventDate,
      'rating': rating,
      'totalReview': totalReview,
      'lat': lat,
      'long': long,
      'prices': prices
    };
  }

  Product.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'].toString();
      sku = json['sku'];
      name = json['name'];
      description = json['description'];
      permalink = json['permalink'];
      price = json['price'];
      regularPrice = json['regularPrice'];
      salePrice = json['salePrice'];
      onSale = json['onSale'];
      inStock = json['inStock'];
      averageRating = json['averageRating'];
      ratingCount = json['ratingCount'];
      idShop = json['idShop'];
      List<String> imgs = [];

      if (json['images'] != null) {
        for (var item in json['images']) {
          imgs.add(item);
        }
      }
      images = imgs;
      imageFeature = json['imageFeature'];
      List<ProductAttribute> attrs = [];

      if (json['attributes'] != null) {
        for (var item in json['attributes']) {
          attrs.add(ProductAttribute.fromLocalJson(item));
        }
      }

      attributes = attrs;
      categoryId = "${json['categoryId']}";
      stockQuantity = json['stock_quantity'];
      if (json['store'] != null) {
        store = Store.fromLocalJson(json['store']);
      }

      ///----FluxStore Listing----///

      distance = json['distance'];
      pureTaxonomies = json['pureTaxonomies'];
      reviews = json['reviews'];
      featured = json['featured'];
      verified = json['verified'];
      tagLine = json['tagLine'];
      priceRange = json['priceRange'];
      categoryName = json['categoryName'];
      hours = json['hours'];
      location = json['location'];
      phone = json['phone'];
      facebook = json['facebook'];
      email = json['email'];
      website = json['website'];
      skype = json['skype'];
      whatsapp = json['whatsapp'];
      youtube = json['youtube'];
      twitter = json['twitter'];
      instagram = json['instagram'];
      eventDate = json['eventDate'];
      rating = json['rating'];
      totalReview = json['totalReview'];
      lat = json['lat'];
      long = json['long'];
      prices = json['prices'];
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
  }

  @override
  String toString() => 'Product { id: $id name: $name }';

  /// Get product ID from mix String productID-ProductVariantID
  static String cleanProductID(productString) {
    if (productString.contains("-")) {
      return productString.split("-")[0].toString();
    } else {
      return productString.toString();
    }
  }

  ///----FLUXSTORE LISTING----////
  Product.fromListingJson(Map<String, dynamic> json) {
    try {
      id = Tools.getValueByKey(json, DataMapping().ProductDataMapping["id"]).toString();
      name = HtmlUnescape()
          .convert(Tools.getValueByKey(json, DataMapping().ProductDataMapping["title"]));
      description =
          Tools.getValueByKey(json, DataMapping().ProductDataMapping["description"]);
      permalink = Tools.getValueByKey(json, DataMapping().ProductDataMapping["link"]);

      distance = Tools.getValueByKey(json, DataMapping().ProductDataMapping["distance"]);

      pureTaxonomies =
          Tools.getValueByKey(json, DataMapping().ProductDataMapping["pureTaxonomies"]);

      final rate = Tools.getValueByKey(json, DataMapping().ProductDataMapping["rating"]);

      averageRating = rate != null
          ? double.parse(double.parse(double.parse(rate.toString()).toString())
              .toStringAsFixed(1))
          : 0.0;

      regularPrice =
          Tools.getValueByKey(json, DataMapping().ProductDataMapping["regularPrice"]);
      price = Tools.getValueByKey(json, DataMapping().ProductDataMapping["priceRange"]);

      type = Tools.getValueByKey(json, DataMapping().ProductDataMapping["type"]);
      categoryName = type;
      rating = Tools.getValueByKey(json, DataMapping().ProductDataMapping["rating"]);
      rating = rating ?? '0.0';

      final reviews =
          Tools.getValueByKey(json, DataMapping().ProductDataMapping["totalReview"]);
      totalReview = reviews != null && reviews != false
          ? int.parse(reviews.toString())
          : 0;
      ratingCount = totalReview;

      location = Tools.getValueByKey(json, DataMapping().ProductDataMapping["address"]);
      final la = Tools.getValueByKey(json, DataMapping().ProductDataMapping["lat"]);
      final lo = Tools.getValueByKey(json, DataMapping().ProductDataMapping["lng"]);
      lat = la != null ? double.parse(la.toString()) : null;
      long = lo != null ? double.parse(lo.toString()) : null;

      phone = Tools.getValueByKey(json, DataMapping().ProductDataMapping["phone"]);
      email = Tools.getValueByKey(json, DataMapping().ProductDataMapping["email"]);
      skype = Tools.getValueByKey(json, DataMapping().ProductDataMapping["skype"]);
      website = Tools.getValueByKey(json, DataMapping().ProductDataMapping["website"]);
      whatsapp = Tools.getValueByKey(json, DataMapping().ProductDataMapping["whatsapp"]);
      facebook = Tools.getValueByKey(json, DataMapping().ProductDataMapping["facebook"]);
      twitter = Tools.getValueByKey(json, DataMapping().ProductDataMapping["twitter"]);
      youtube = Tools.getValueByKey(json, DataMapping().ProductDataMapping["youtube"]);
      instagram = Tools.getValueByKey(json, DataMapping().ProductDataMapping["instagram"]);
      tagLine = Tools.getValueByKey(json, DataMapping().ProductDataMapping["tagLine"]);
      eventDate = Tools.getValueByKey(json, DataMapping().ProductDataMapping["eventDate"]);
      priceRange = Tools.getValueByKey(json, DataMapping().ProductDataMapping["priceRange"]);
      List<String> list = [];
      final gallery = Tools.getValueByKey(json, DataMapping().ProductDataMapping["gallery"]);
      if (gallery != null) {
        if (gallery is Map) {
          var keys = List<String>.from(gallery.keys);
          for (var item in keys) {
            if (gallery['$item'].contains('http')) {
              list.add(gallery['$item']);
            } else {
              list.add(item);
            }
          }
        } else {
          gallery.forEach((item) {
            if (item is Map) {
              list.add(item['media_details']['sizes']['medium']['source_url']);
            } else {
              list.add(item);
            }
          });
        }
      }
      var defaultImages =
          Tools.getValueByKey(json, DataMapping().ProductDataMapping["featured_media"]);
      if (defaultImages is String) {
        if (defaultImages == null) {
          imageFeature = list.isNotEmpty ? list[0] : kDefaultImage;
        } else {
          imageFeature = defaultImages.isEmpty
              ? list.isNotEmpty ? list[0] : kDefaultImage
              : defaultImages;
        }
      } else {
        if (defaultImages != null) {
          imageFeature = defaultImages.isNotEmpty
              ? defaultImages[0]
              : list.isNotEmpty ? list[0] : kDefaultImage;
        } else {
          imageFeature = list.isNotEmpty ? list[0] : kDefaultImage;
        }
      }

      images = list;
      final items = Tools.getValueByKey(json, DataMapping().ProductDataMapping["menu"]);
      if (items != null && items.length > 0) {
        for (var i = 0; i < items.length; i++) {
          prices.add(MenuPrice.fromJson(items[i]));
        }
      }

      ///Set other attributes that not relate to Listing to be unusable

    } catch (err) {
      printLog('err when parsed json Listing $err');
    }
  }

  ///----FLUXSTORE LISTING----////
}

class BookingDate1 {
  int value;
  String unit;

  BookingDate1.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    unit = json['unit'];
  }
}
