import 'menu.dart';

class MenuPrice {
  String title;
  List<Menu> menu = [];

  Map<String, dynamic> toJson() {
    return {'title': title, 'menu': menu};
  }

  MenuPrice.fromJson(Map<String, dynamic> json) {
    title = json["menu_title"];
    List elements = json["menu_elements"] is Map
        ? json["menu_elements"].values.toList()
        : json["menu_elements"];
    if (elements != null && elements.isNotEmpty) {
      for (var i = 0; i < elements.length; i++) {
        menu.add(Menu.fromJson(elements[i]));
      }
    }
  }
}
