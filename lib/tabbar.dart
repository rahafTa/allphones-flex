import 'dart:async';


import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'menu.dart';
import 'models/app_model.dart';
import 'models/cart/cart_model.dart';
import 'models/search_model.dart';
import 'pages/index.dart';
import 'route.dart';
import 'screens/index.dart'
    show
        CartScreen,
        PostScreen,
        CategoriesScreen,
        WishListScreen,
        HomeScreen,
        NotificationScreen,
        StaticSite,
        UserScreen,
        WebViewScreen;
import 'services/index.dart';
import 'widgets/blog/slider_list.dart';
import 'widgets/common/auto_hide_keyboard.dart';
import 'widgets/icons/feather.dart';
import 'widgets/layout/adaptive.dart';
import 'widgets/layout/layout_web.dart';

const int tabCount = 3;
const int turnsToRotateRight = 1;
const int turnsToRotateLeft = 3;

class MainTabControlDelegate {
  int index;
  Function(String nameTab) changeTab;
  Function(int index) tabAnimateTo;

  static MainTabControlDelegate _instance;

  static MainTabControlDelegate getInstance() {
    return _instance ??= MainTabControlDelegate._();
  }

  MainTabControlDelegate._();
}

class MainTabs extends StatefulWidget {
  MainTabs({Key key}) : super(key: key);

  @override
  MainTabsState createState() => MainTabsState();
}

class MainTabsState extends State<MainTabs>
    with
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin
         {
  final _auth = FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey(debugLabel: 'Dashboard');
  final List<Widget> _tabView = [];
  final navigators = Map<int, GlobalKey<NavigatorState>>();

  var tabData;

  Map saveIndexTab = Map();

  firebase_auth.User loggedInUser;

  TabController tabController;

  bool isAdmin = false;
  bool isFirstLoad = false;
  bool isShowCustomDrawer = false;

  StreamSubscription _subOpenNativeDrawer;
  StreamSubscription _subCloseNativeDrawer;
  StreamSubscription _subOpenCustomDrawer;
  StreamSubscription _subCloseCustomDrawer;

  bool get isBigScreen => isDisplayDesktop(context);


  @override
  void afterFirstLayout(BuildContext context) {
    loadTabBar(context);
  }

  Widget tabView(Map<String, dynamic> data) {
    switch (data['layout']) {
      case 'category':
        return CategoriesScreen(
          key: const Key("category"),
          layout: data['categoryLayout'],
          categories: data['categories'],
          images: data['images'],
          showChat: data['showChat'],
          showSearch: data['showSearch'] ?? true,
        );
      case 'search':
        {
          return AutoHideKeyboard(
            child: ChangeNotifierProvider<SearchModel>(
              create: (context) => SearchModel(),
              child: Services().widget.renderSearchScreen(
                    context,
                    showChat: data['showChat'],
                  ),
            ),
          );
        }

      case 'cart':
        return CartScreen(showChat: data['showChat']);
      case 'profile':
        return UserScreen(
            settings: data['settings'],
            background: data['background'],
            showChat: data['showChat']);
      case 'blog':
        return HorizontalSliderList(config: data);
      case 'wishlist':
        return WishListScreen(canPop: false, showChat: data['showChat']);
      case 'page':
        return WebViewScreen(
            title: data['title'], url: data['url'], showChat: data['showChat']);
      case 'html':
        return StaticSite(data: data['data'], showChat: data['showChat']);
      case 'static':
        return StaticPage(data: data['data'], showChat: data['showChat']);
      case 'postScreen':
        return PostScreen(
            pageId: data['pageId'],
            pageTitle: data['pageTitle'],
            isLocatedInTabbar: true,
            showChat: data['showChat']);

      /// Story Screen
      // case 'story':
      //   return StoryWidget(
      //     config: data,
      //     isFullScreen: true,
      //     onTapStoryText: (cfg) {
      //       Utils.onTapNavigateOptions(context: context, config: cfg);
      //     },
      //   );

      /// vendor screens
      case 'vendors':
        return Services().widget.renderVendorCategoriesScreen(data);

      case 'map':
        return Services().widget.renderMapScreen();

      /// Default Screen
      case 'dynamic':
      default:
        return const HomeScreen();
    }
  }

  void changeTab(String nameTab) {
    if (saveIndexTab[nameTab] != null) {
      tabController?.animateTo(saveIndexTab[nameTab]);
    } else {
      Navigator.of(context, rootNavigator: true).pushNamed("/$nameTab");
    }
  }

  void loadTabBar(context) {
    tabData = List.from(
        Provider.of<AppModel>(context, listen: false).appConfig['TabBar']);

    for (var i = 0; i < tabData.length; i++) {
      Map<String, dynamic> _dataOfTab = Map.from(tabData[i]);
      saveIndexTab[_dataOfTab['layout']] = i;
      navigators[i] = GlobalKey<NavigatorState>();
      _tabView.add(Navigator(
        key: navigators[i],
        onGenerateRoute: (RouteSettings settings) {
          if (settings.name == Navigator.defaultRouteName) {
            return MaterialPageRoute(
              builder: (context) => tabView(_dataOfTab),
              fullscreenDialog: true,
              settings: settings,
            );
          }
          return Routes.getRouteGenerate(settings);
        },
      ));
    }

    setState(() {
      tabController = TabController(length: _tabView.length, vsync: this);
    });

    if (MainTabControlDelegate.getInstance().index != null) {
      tabController.animateTo(MainTabControlDelegate.getInstance().index);
    } else {
      MainTabControlDelegate.getInstance().index = 0;
    }

    // Load the Design from FluxBuilder
    tabController.addListener(() {
      eventBus.fire('tab_${tabController.index}');
      MainTabControlDelegate.getInstance().index = tabController.index;
    });
  }

  Future<void> getCurrentUser() async {
    try {
      //Provider.of<UserModel>(context).getUser();
      final user = await _auth.currentUser;
      if (user != null) {
        setState(() {
          loggedInUser = user;
        });
      }
    } catch (e) {
      printLog("[tabbar] getCurrentUser error ${e.toString()}");
    }
  }

  bool checkIsAdmin() {
    if (loggedInUser.email == adminEmail) {
      isAdmin = true;
    } else {
      isAdmin = false;
    }
    return isAdmin;
  }

  @override
  void initState() {
    printLog("[Dashboard] init");
    if (!kIsWeb) {
      getCurrentUser();
    }
    setupListenEvent();
    MainTabControlDelegate.getInstance().changeTab = changeTab;
    MainTabControlDelegate.getInstance().tabAnimateTo = (int index) {
      tabController?.animateTo(index);
    };
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance
        .addPostFrameCallback((_) async { await afterFirstLayout(context);});
    //loadTabBar(context);

  }

  @override
  void didChangeDependencies() {
    isShowCustomDrawer = isBigScreen;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    tabController?.dispose();
    WidgetsBinding.instance.removeObserver(this);
    _subOpenNativeDrawer?.cancel();
    _subCloseNativeDrawer?.cancel();
    _subOpenCustomDrawer?.cancel();
    _subCloseCustomDrawer?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      // went to Background
    }
    if (state == AppLifecycleState.resumed) {
      // came back to Foreground
      final appModel = Provider.of<AppModel>(context, listen: false);
      if (appModel.deeplink?.isNotEmpty ?? false) {
        if (appModel.deeplink['screen'] == 'NotificationScreen') {
          appModel.deeplink = null;
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NotificationScreen()),
          );
        }
      }
    }

    super.didChangeAppLifecycleState(state);
  }

  void setupListenEvent() {
    _subOpenNativeDrawer = eventBus.on<EventOpenNativeDrawer>().listen((event) {
      if (!_scaffoldKey.currentState.isDrawerOpen) {
        _scaffoldKey.currentState.openDrawer();
      }
    });
    _subCloseNativeDrawer =
        eventBus.on<EventCloseNativeDrawer>().listen((event) {
      if (_scaffoldKey.currentState.isDrawerOpen) {
        _scaffoldKey.currentState.openEndDrawer();
      }
    });
    _subOpenCustomDrawer = eventBus.on<EventOpenCustomDrawer>().listen((event) {
      setState(() {
        isShowCustomDrawer = true;
      });
    });
    _subCloseCustomDrawer =
        eventBus.on<EventCloseCustomDrawer>().listen((event) {
      setState(() {
        isShowCustomDrawer = false;
      });
    });
  }

  Future<bool> handleWillPopScopeRoot() {
    // Check pop navigator current tab
    final currentNavigator = navigators[tabController.index];
    if (currentNavigator.currentState.canPop()) {
      currentNavigator.currentState.pop();
      return Future.value(false);
    }
    // Check pop root navigator
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
      return Future.value(false);
    }
    if (tabController.index != 0) {
      tabController.animateTo(0);
      return Future.value(false);
    } else {
      return showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(S.of(context).areYouSure,style: TextStyle(fontFamily : 'DroidKuffi')),
              content: Text(S.of(context).doYouWantToExitApp),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text(S.of(context).no,style: TextStyle(fontFamily: 'DroidKuffi'),),
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text(S.of(context).yes,style: TextStyle(fontFamily: 'DroidKuffi')),
                ),
              ],
            ),
          ) ??
          false;
    }
  }

  @override
  Widget build(BuildContext context) {
    printLog('[tabbar] ============== tabbar.dart DASHBOARD ==============');
    printLog(
        '[Resolution Screen]: ${MediaQuery.of(context).size.width} x ${MediaQuery.of(context).size.height}');
    Utils.setStatusBarWhiteForeground(false);

    if (_tabView.isEmpty) {
      return Container(
        color: Colors.white,
        child: kLoadingWidget(context),
      );
    }

    return renderBody(context);
  }

  Widget renderBody(BuildContext context) {
    var appModel = Provider.of<AppModel>(context);
    var isDarkTheme = appModel.darkTheme ?? false;

    final screenSize = MediaQuery.of(context).size;
    final appSetting = Provider.of<AppModel>(context).appConfig['Setting'];
    final colorTabbar = appSetting['ColorTabbar'] != null
        ? HexColor(appSetting['ColorTabbar'])
        : false;

    // TabBarView
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
//      resizeToAvoidBottomPadding: false,
      body: WillPopScope(
        onWillPop: handleWillPopScopeRoot,
        child: LayoutWebCustom(
          menu: MenuBar(),
          content: ListenableProvider.value(
            value: tabController,
            child: Consumer(
              builder: (context, TabController controller, child) {
                return Stack(
                  fit: StackFit.expand,
                  children: List.generate(
                    _tabView.length,
                    (index) {
                      final active = controller.index == index;
                      return Offstage(
                        offstage: !active,
                        child: TickerMode(
                          child: _tabView[index],
                          enabled: active,
                        ),
                      );
                    },
                  ).toList(),
                );
              },
            ),
          ),
        ),
      ),
      drawer: isBigScreen ? null : Drawer(child: MenuBar()),
      bottomNavigationBar: Container(
        color: colorTabbar == false
            ? Theme.of(context).backgroundColor
            : colorTabbar,
        child: SafeArea(
          top: false,
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 600),
            curve: Curves.easeInOutQuint,
            height: isShowCustomDrawer ? 0 : null,
            constraints: const BoxConstraints(
              maxHeight: 60,
            ),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.black12, width: 0.5),
              ),
            ),
            width: screenSize.width,
            child: FittedBox(
              child: Container(
                width: screenSize.width /
                    (1.8 / (screenSize.height / screenSize.width)),
                child:
                Container(
                  child: TabBar(
                  controller: tabController,
                  tabs: renderTabbar(),
                  isScrollable: false,
                  labelColor: colorTabbar == false
                      ? Theme.of(context).primaryColor
                      : Colors.white,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorPadding: const EdgeInsets.all(1.0),
                  indicatorColor: colorTabbar == false
                      ? Theme.of(context).primaryColor
                      : Colors.white,

                ),
//                  color:
//                isDarkTheme?
//                Colors.blueGrey.shade900 :
//                Colors.grey.shade50 ,

                   )

              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> renderTabbar() {

    final isTablet = Tools.isTablet(MediaQuery.of(context));
    var totalCart = Provider.of<CartModel>(context).totalCartQuantity;
    final tabData = Provider.of<AppModel>(context, listen: false)
        .appConfig['TabBar'] as List;

    final appSetting = Provider.of<AppModel>(context).appConfig['Setting'];

    final colorIcon = appSetting['ColorTabbar'] != null
        ? Colors.white
        : Theme.of(context).accentColor;

    List<Widget> list = [];

    tabData.forEach((item) {
      var icon = !item["icon"].contains('/')
          ?


      Icon(
        featherIcons[item["icon"]],
        color: colorIcon,
        size: 12,
      )


          : (item["icon"].contains('http')
              ?
      Image.network(
        item["icon"],
        color: colorIcon,
        width: 14,
      )

              :

          Container(margin:EdgeInsets.only(top:4),child:
           Column(children: [
            Image.asset(
              item["icon"],
              color: colorIcon,
              width: 20,
            ),


          Expanded(child:
             Text(item["type"].toString(),style: TextStyle(fontFamily: 'DroidKuffi')
             )
              ,),
           ],))

      );

      if (item["layout"] == "cart") {
        icon = Stack(
          children: <Widget>[
            Column(children: [
              Container(
                width: 250,
                height: 28,
                child: icon,
              ),

      Expanded(child:
              Center(
                child:
                Text(item["type"].toString(),style: TextStyle(fontFamily: 'DroidKuffi'),),
              ),
      ),
            ],),

            if (totalCart > 0)
              Positioned(
                right: 15,
                top: 0,
                child: Container(
                  padding: const EdgeInsets.all(1),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  constraints: const BoxConstraints(
                    minWidth: 16,
                    minHeight: 16,
                  ),
                  child: Text(
                    totalCart.toString(),
                    style: TextStyle(
                      fontFamily: 'DroidKuffi',
                      color: Colors.white,
                      fontSize: isTablet ? 14 : 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
          ],
        );
      }

      if (item["label"] != null) {
        list.add(Tab(icon: icon, text: item["label"]));
      } else {
        list.add(Tab(icon: icon));
      }
    });

    return list;
  }
}
