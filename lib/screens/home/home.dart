import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';
import 'package:universal_platform/universal_platform.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/app_model.dart';
import '../../models/point_model.dart';
import '../../models/user_model.dart';
import '../../services/index.dart';
import '../../widgets/home/background.dart';
import '../../widgets/home/index.dart';
import 'deeplink_item.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen();

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with
        AutomaticKeepAliveClientMixin<HomeScreen> {
  @override
  bool get wantKeepAlive => true;

  Uri _latestUri;
  StreamSubscription _sub;
  int itemId;

  @override
  void dispose() {
    printLog("[Home] dispose");
    _sub?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    printLog("[Home] initState");

    initPlatformState();
    super.initState();
  }

  Future<void> initPlatformState() async {
    if (UniversalPlatform.isAndroid || UniversalPlatform.isIOS) {
      await initPlatformStateForStringUniLinks();
    }
  }

  Future<void> initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
    _sub = getLinksStream().listen((String link) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        try {
          if (link != null) _latestUri = Uri.parse(link);
          setState(() {
            itemId = int.parse(_latestUri.path.split('/')[1]);
          });

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ItemDeepLink(
                itemId: itemId,
              ),
            ),
          );
        } on FormatException {
          printLog('[initPlatformStateForStringUniLinks] error');
        }
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
      });
    });

    getLinksStream().listen((String link) {
      printLog('got link: $link');
    }, onError: (err) {
      printLog('got err: $err');
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    printLog("[Home] afterFirstLayout");

    final userModel = Provider.of<UserModel>(context, listen: false);

    if (userModel.user != null &&
        userModel.user.cookie != null &&
        kAdvanceConfig["EnableSyncCartFromWebsite"]) {
      Services().widget.syncCartFromWebsite(userModel.user.cookie, context);
    }

    if (userModel.user != null && userModel.user.cookie != null) {
      Provider.of<PointModel>(context, listen: false).getMyPoint(
          Provider.of<UserModel>(context, listen: false).user.cookie);
    }

    DynamicLinkService();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    printLog("[Home] build");
    return SafeArea(
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          if (value.appConfig == null) {
            return kLoadingWidget(context);
          }
          bool isStickyHeader = value.appConfig["Setting"] != null
              ? (value.appConfig["Setting"]["StickyHeader"] ?? false)
              : false;

          return Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            body: Stack(
              children: <Widget>[
                if (value.appConfig['Background'] != null)
                  isStickyHeader
                      ? SafeArea(
                          child: HomeBackground(
                            config: value.appConfig['Background'],
                          ),
                        )
                      : HomeBackground(config: value.appConfig['Background']),
                HomeLayout(
                  isPinAppBar: isStickyHeader,
                  isShowAppbar:
                      value.appConfig['HorizonLayout'][0]['layout'] == 'logo',
                  configs: value.appConfig,
                  key: Key(value.langCode),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
