import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart' show AppModel, Category, CategoryModel;
import '../../widgets/cardlist/index.dart';
import '../custom/smartchat.dart';
import 'card.dart';
import 'column.dart';
import 'grid_category.dart';
import 'side_menu.dart';
import 'sub.dart';

class CategoriesScreen extends StatefulWidget {
  final String layout;
  final List<dynamic> categories;
  final List<dynamic> images;
  final bool showChat;
  final bool showSearch;
  CategoriesScreen(
      {Key key,
      this.layout,
      this.categories,
      this.images,
      this.showChat,
      this.showSearch = true})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CategoriesScreenState();
  }
}

class CategoriesScreenState extends State<CategoriesScreen>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  FocusNode _focus;
  bool isVisibleSearch = false;
  String searchText;
  var textController = TextEditingController();

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    animation = Tween<double>(begin: 0, end: 60).animate(controller);
    animation.addListener(() {
      setState(() {});
    });

    _focus = FocusNode();
    _focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (_focus.hasFocus && animation.value == 0) {
      controller.forward();
      setState(() {
        isVisibleSearch = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final category = Provider.of<CategoryModel>(context);
    final bool showChat = widget.showChat ?? false;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: showChat
          ? SmartChat(
              margin: EdgeInsets.only(
                right: Provider.of<AppModel>(context, listen: false).langCode ==
                        'ar'
                    ? 30.0
                    : 0.0,
              ),
            )
          : Container(),
      body: ListenableProvider.value(
        value: category,
        child: Consumer<CategoryModel>(
          builder: (context, value, child) {
            if (value.isLoading) {
              return kLoadingWidget(context);
            }

            if (value.categories == null) {
              return Container(
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.center,
                child: Text(S.of(context).dataEmpty,style: TextStyle(fontFamily: 'DroidKuffi',),),
              );
            }

            List<Category> categories = value.categories;

            return SafeArea(
              child: ['grid', 'column', 'sideMenu', 'subCategories']
                      .contains(widget.layout)
                  ? Column(
                      children: <Widget>[
                        renderHeader(),
                        Expanded(
                          child: renderCategories(categories),
                        )
                      ],
                    )
                  : ListView(
                      children: <Widget>[
                        renderHeader(),
                        renderCategories(categories)
                      ],
                    ),
            );
          },
        ),
      ),
    );
  }

  Widget renderHeader() {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      width: screenSize.width,
      child: FittedBox(
        fit: BoxFit.cover,
        child: Container(
          width:
              screenSize.width / (2 / (screenSize.height / screenSize.width)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                child: Text(
                  S.of(context).category,
                  style: const TextStyle(
                      fontFamily: 'DroidKuffi',
                      fontSize: 27, fontWeight: FontWeight.bold),
                ),
                padding: const EdgeInsets.only(
                    top: 10, left: 10, bottom: 20, right: 10),
              ),
              if (widget.showSearch)
                IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Theme.of(context).accentColor.withOpacity(0.6),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(RouteList.categorySearch);
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderCategories(value) {
    switch (widget.layout) {
      case 'card':
        return CardCategories(value);
      case 'column':
        return ColumnCategories(value);
      case 'subCategories':
        return SubCategories(value);
      case 'sideMenu':
        return SideMenuCategories(value);
      case 'animation':
        return HorizonMenu(value);
      case 'grid':
        return GridCategory(
          value,
          icons: widget.images,
        );
      default:
        return HorizonMenu(value);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
