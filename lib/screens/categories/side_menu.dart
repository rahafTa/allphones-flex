import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/index.dart' show AppModel, Category, Product;
import '../../services/index.dart';
import '../../widgets/product/product_list.dart';

class SideMenuCategories extends StatefulWidget {
  final List<Category> categories;

  SideMenuCategories(this.categories);

  @override
  State<StatefulWidget> createState() => SideMenuCategoriesState();
}

class SideMenuCategoriesState extends State<SideMenuCategories>
    {
  int selectedIndex = 0;
  int page = 1;
  bool isEnd = false;
  bool isFetching = true;
  CancelableCompleter completer;
  final Services _service = Services();
  List<Category> _categories = [];
  List<Product> products = [];

  @override
  void initState() {
    getParentCategory();
    super.initState();
    onRefresh();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    onRefresh();
  }

  void onLoadMore() async {
    setState(() {
      isFetching = true;
    });
    if (hasChildren(_categories[selectedIndex].id)) {
      var category = getSubCategories(_categories[selectedIndex].id);
      for (var item in category) {
        setState(() {
          completer = CancelableCompleter();
        });
        completer.complete(_service.fetchProductsByCategory(
            lang: Provider.of<AppModel>(context, listen: false).langCode,
            categoryId: item.id,
            page: page + 1));
        await completer.operation.then((value) {
          setState(() {
            products = [...products, ...value];
            isFetching = false;
            page = page + 1;
          });
        });
      }
    } else {
      setState(() {
        completer = CancelableCompleter();
      });
      completer.complete(_service.fetchProductsByCategory(
          lang: Provider.of<AppModel>(context, listen: false).langCode,
          categoryId: widget.categories[selectedIndex].id,
          page: page + 1));
      await completer.operation.then((value) {
        if (value.length < 2) {
          setState(() {
            isEnd = true;
          });
        }
        setState(() {
          products = [...products, ...value];
          isFetching = false;
          page = page + 1;
        });
      });
    }
  }

  void onRefresh() async {
    setState(() {
      isFetching = true;
      products = [];
    });
    if (hasChildren(_categories[selectedIndex].id)) {
      var category = getSubCategories(_categories[selectedIndex].id);
      for (var item in category) {
        setState(() {
          completer = CancelableCompleter();
        });
        completer.complete(_service.fetchProductsByCategory(
            lang: Provider.of<AppModel>(context, listen: false).langCode,
            categoryId: item.id,
            page: 1));
        await completer.operation.then((value) {
          setState(() {
            products = [...products, ...value];
            isFetching = false;
            isEnd = false;
            page = 1;
          });
        });
      }
    } else {
      setState(() {
        completer = CancelableCompleter();
      });
      completer.complete(_service.fetchProductsByCategory(
          lang: Provider.of<AppModel>(context, listen: false).langCode,
          categoryId: widget.categories[selectedIndex].id,
          page: 1));
      await completer.operation.then((value) {
        setState(() {
          products = [...products, ...value];
          isFetching = false;
          isEnd = false;
          page = 1;
        });
      });
    }
  }

  List<Category> getSubCategories(id) {
    return widget.categories.where((o) => o.parent == id).toList();
  }

  bool hasChildren(id) {
    return widget.categories.where((o) => o.parent == id).toList().isNotEmpty;
  }

  void getParentCategory() {
    setState(() {
      _categories =
          widget.categories.where((item) => item.parent == '0').toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final screenSize = MediaQuery.of(context).size;

    return Row(
      children: <Widget>[
        Container(
          width: 100,
          child: ListView.builder(
            itemCount: _categories.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    selectedIndex = index;
                    completer.operation.cancel();
                  });
                  onRefresh();
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 15, bottom: 15, left: 4, right: 4),
                    child: Center(
                      child: Text(
                        _categories[index] != null &&
                                _categories[index].name != null
                            ? _categories[index].name.toUpperCase()
                            : '',
                        style: TextStyle(
                          fontFamily: 'DroidKuffi',
                          fontSize: 10,
                          color: selectedIndex == index
                              ? theme.primaryColor
                              : theme.accentColor,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        Expanded(
          child: ProductList(
            isFetching: isFetching,
            onRefresh: onRefresh,
            onLoadMore: onLoadMore,
            isEnd: isEnd,
            products: products,
            width: screenSize.width - 100,
            padding: 4.0,
            layout: "list",
          ),
        )
      ],
    );
  }
}
