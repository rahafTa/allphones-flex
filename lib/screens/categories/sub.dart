import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/index.dart' show AppModel, Category, Product;
import '../../services/index.dart';
import '../../widgets/product/product_list.dart';

class SubCategories extends StatefulWidget {
  final List<Category> categories;
  SubCategories(this.categories);

  @override
  State<StatefulWidget> createState() {
    return SubCategoriesState();
  }
}

class SubCategoriesState extends State<SubCategories> {
  int selectedIndex = 0;
  int page = 1;
  bool isFetching = true;
  bool isEnd = false;
  List<Product> products = [];
  CancelableCompleter completer = CancelableCompleter();
  final Services _service = Services();

  @override
  void afterFirstLayout(BuildContext context) {
    onRefresh();
  }

  void onLoadMore() async {
    setState(() {
      isFetching = true;
      completer = CancelableCompleter();
    });
    List<Product> _products;
    completer.complete(_service.fetchProductsByCategory(
        lang: Provider.of<AppModel>(context, listen: false).langCode,
        categoryId: widget.categories[selectedIndex].id,
        page: page + 1));
    await completer.operation.then((value) {
      _products = value;
      if (_products.length < 2) {
        setState(() {
          isEnd = true;
        });
      }
      setState(() {
        isFetching = false;
        products = [...products, ..._products];
        page = page + 1;
      });
    });
  }

  void onRefresh() async {
    setState(() {
      isFetching = true;
      completer = CancelableCompleter();
    });
    List<Product> _products;
    completer.complete(_service.fetchProductsByCategory(
        lang: Provider.of<AppModel>(context, listen: false).langCode,
        categoryId: widget.categories[selectedIndex].id,
        page: 1));
    await completer.operation.then((value) {
      _products = value;
      setState(() {
        isFetching = false;
        products = _products;
        isEnd = false;
        page = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    onRefresh();
    final theme = Theme.of(context);

    return Column(
      children: <Widget>[
        Container(
          height: 60,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: widget.categories.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    selectedIndex = index;
                    page = 1;
                    isFetching = true;
                    isEnd = false;
                    products = [];
                    completer.operation.cancel();
                  });
                  onRefresh();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Center(
                    child: Text(widget.categories[index].name,
                        style: TextStyle(
                            fontFamily: 'DroidKuffi',
                            fontSize: 18,
                            color: selectedIndex == index
                                ? theme.primaryColor
                                : theme.hintColor)),
                  ),
                ),
              );
            },
          ),
        ),
        Expanded(
          child: LayoutBuilder(
            builder: (context, constraints) {
              return ProductList(
                width: constraints.maxWidth,
                products: products,
                isEnd: isEnd,
                isFetching: isFetching,
                onLoadMore: onLoadMore,
                onRefresh: onRefresh,
              );
            },
          ),
        )
      ],
    );
  }
}
