import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'app_init.dart';
import 'common/constants.dart';
import 'common/styles.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'models/advertisement_model.dart';
import 'models/app_model.dart';
import 'models/blog_model.dart';
import 'models/cart/cart_model.dart';
import 'models/category_model.dart';
import 'models/entities/notification.dart';
import 'models/filter_attribute_model.dart';
import 'models/filter_tags_model.dart';
import 'models/order_model.dart';
import 'models/payment_method_model.dart';
import 'models/point_model.dart';
import 'models/product_model.dart';
import 'models/recent_product_model.dart';
import 'models/shipping_method_model.dart';
import 'models/tag_model.dart';
import 'models/tax_model.dart';
import 'models/user_model.dart';
import 'models/vendor/store_model.dart';
import 'models/vendor/vendor_shipping_model.dart';
import 'models/wishlist_model.dart';
import 'route.dart';
import 'routes/route_observer.dart';
import 'tabbar.dart';
import 'widgets/common/internet_connectivity.dart';
import 'widgets/firebase/firebase_analytics_wapper.dart';
import 'widgets/firebase/firebase_cloud_messaging_wapper.dart';
import 'widgets/firebase/one_signal_wapper.dart';

class App extends StatefulWidget {
  App();

  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> implements FirebaseCloudMessagingDelegate {
  final _app = AppModel();
  final _product = ProductModel();
  final _wishlist = WishListModel();
  final _shippingMethod = ShippingMethodModel();
  final _paymentMethod = PaymentMethodModel();
  final _advertisementModel = Ads();
  final _order = OrderModel();
  final _recent = RecentModel();
  final _blog = BlogModel();
  final _user = UserModel();
  final _filterModel = FilterAttributeModel();
  final _filterTagModel = FilterTagModel();
  final _categoryModel = CategoryModel();
  final _tagModel = TagModel();
  final _taxModel = TaxModel();
  final _pointModel = PointModel();

  // ---------- Vendor -------------
  StoreModel _storeModel;
  VendorShippingMethodModel _vendorShippingMethodModel;

  CartInject cartModel = CartInject();
  bool isFirstSeen = false;
  bool isLoggedIn = false;

  FirebaseAnalyticsAbs firebaseAnalyticsAbs;

  @override
  void initState() {
    printLog("[AppState] initState");

    if (kIsWeb) {
      printLog("[AppState] init WEB");
      firebaseAnalyticsAbs = FirebaseAnalyticsWeb();
    } else {
      firebaseAnalyticsAbs = FirebaseAnalyticsWapper()..init();

      Future.delayed(
        const Duration(seconds: 6),
        () {
          printLog("[AppState] init mobile modules ..");

//          MyConnectivity.instance.initialise();
//          MyConnectivity.instance.myStream.listen((onData) {
//            printLog("[App] internet issue change: $onData");
//
//            /// disable by causing loading performance
//            // if (MyConnectivity.instance.isIssue(onData)) {
//            //   if (MyConnectivity.instance.isShow == false) {
//            //     MyConnectivity.instance.isShow = true;
//            //     showDialogNotInternet(context).then((onValue) {
//            //       MyConnectivity.instance.isShow = false;
//            //       printLog("[showDialogNotInternet] dialog closed $onValue");
//            //     });
//            //   }
//            // } else {
//            //   if (MyConnectivity.instance.isShow == true) {
//            //     Navigator.of(context).pop();
//            //     MyConnectivity.instance.isShow = false;
//            //   }
//            // }
//          });

          FirebaseCloudMessagagingWapper()
            ..init()
            ..delegate = this;

          OneSignalWapper()..init();
          printLog("[AppState] register modules .. DONE");
        },
      );
    }

    super.initState();
  }

  void _saveMessage(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      _app.deeplink = message['data'];
    }

    FStoreNotification a = FStoreNotification.fromJsonFirebase(message);
    final id = message['notification'] != null
        ? message['notification']['tag']
        : message['data']['google.message_id'];

    a.saveToLocal(id);
  }

  @override
  void onLaunch(Map<String, dynamic> message) {
    printLog('[app.dart] onLaunch Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onMessage(Map<String, dynamic> message) {
    printLog('[app.dart] onMessage Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onResume(Map<String, dynamic> message) {
    printLog('[app.dart] onResume Pushnotification: $message');

    _saveMessage(message);
  }

  /// Build the App Theme
  ThemeData getTheme(context) {
    printLog("[AppState] build Theme");

    var appModel = Provider.of<AppModel>(context);
    var isDarkTheme = appModel.darkTheme ?? false;

    if (appModel.appConfig == null) {
      /// This case is loaded first time without config file
      return buildLightTheme(appModel.langCode);
    }

    if (isDarkTheme) {
      return buildDarkTheme(appModel.langCode).copyWith(
        primaryColor: HexColor(
          appModel.appConfig["Setting"]["MainColor"],
        ),

      );
    }
    return buildLightTheme(appModel.langCode).copyWith(
      primaryColor:
//      Colors.red
      HexColor(appModel.appConfig["Setting"]["MainColor"]),
    );
  }

  @override
  Widget build(BuildContext context) {
    printLog("[AppState] build");
    return ChangeNotifierProvider<AppModel>(
      create: (context) => _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          if (value.vendorType == VendorType.multi &&
              _storeModel == null &&
              _vendorShippingMethodModel == null) {
            _storeModel = StoreModel();
            _vendorShippingMethodModel = VendorShippingMethodModel();
          }
          return MultiProvider(
            providers: [
              Provider<ProductModel>.value(value: _product),
              Provider<WishListModel>.value(value: _wishlist),
              Provider<ShippingMethodModel>.value(value: _shippingMethod),
              Provider<PaymentMethodModel>.value(value: _paymentMethod),
              Provider<OrderModel>.value(value: _order),
              Provider<RecentModel>.value(value: _recent),
              Provider<UserModel>.value(value: _user),
              ChangeNotifierProvider<FilterAttributeModel>(
                  create: (_) => _filterModel),
              ChangeNotifierProvider<FilterTagModel>(
                  create: (_) => _filterTagModel),
              ChangeNotifierProvider<CategoryModel>(
                  create: (_) => _categoryModel),
              ChangeNotifierProvider(create: (_) => _tagModel),
              ChangeNotifierProvider(create: (_) => cartModel.model),
              ChangeNotifierProvider(create: (_) => BlogModel()),
              ChangeNotifierProvider(create: (_) => _blog),
              ChangeNotifierProvider(create: (_) => _advertisementModel),
              Provider<TaxModel>.value(value: _taxModel),
              if (value.vendorType == VendorType.multi) ...[
                ChangeNotifierProvider<StoreModel>(create: (_) => _storeModel),
                Provider<VendorShippingMethodModel>.value(
                    value: _vendorShippingMethodModel),
              ],
              Provider<PointModel>.value(value: _pointModel),
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              navigatorKey: App.navigatorKey,
              locale: Locale(value.langCode, ""),
              navigatorObservers: [
                MyRouteObserver(),
                ...firebaseAnalyticsAbs.getMNavigatorObservers()
              ],
              localizationsDelegates: const [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              home: Scaffold(
                body: AppInit(
                  onNext: () => MainTabs(),
                ),
              ),
              routes: Routes.getAll(),
              onGenerateRoute: Routes.getRouteGenerate,
              theme: getTheme(context),
            ),
          );
        },
      ),
    );
  }
}
