import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart'
    show
        AppModel,
        Category,
        TagModel,
        CategoryModel,
        FilterAttributeModel,
        ProductModel;
import '../../services/service_config.dart';
import '../common/tree_view.dart';
import '../layout/adaptive.dart';
import 'category_item.dart';
import 'filter_option_item.dart';

class BackdropMenu extends StatefulWidget {
  final Function onFilter;
  final String categoryId;
  final String tagId;

  const BackdropMenu({
    Key key,
    this.onFilter,
    this.categoryId,
    this.tagId,
  }) : super(key: key);

  @override
  _BackdropMenuState createState() => _BackdropMenuState();
}

class _BackdropMenuState extends State<BackdropMenu> {
  double mixPrice = 0.0;
  double maxPrice = kMaxPriceFilter / 2;
  String categoryId = '-1';
  String tagId = '-1';
  String currentSlug;
  int currentSelectedAttr = -1;

  @override
  void initState() {
    super.initState();
    categoryId = widget.categoryId;
    tagId = widget.tagId;
  }

  @override
  Widget build(BuildContext context) {
    final category = Provider.of<CategoryModel>(context);
    final tag = Provider.of<TagModel>(context);
    final selectLayout = Provider.of<AppModel>(context).productListLayout;
    final currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final filterAttr = Provider.of<FilterAttributeModel>(context);

    categoryId = Provider.of<ProductModel>(context).categoryId;

    Function _onFilter = (categoryId, tagId) => widget.onFilter(
          minPrice: mixPrice,
          maxPrice: maxPrice,
          categoryId: categoryId,
          tagId: tagId,
          attribute: currentSlug,
          currentSelectedTerms: filterAttr.lstCurrentSelectedTerms,
        );

    return ListenableProvider.value(
      value: category,
      child: Consumer<CategoryModel>(
        builder: (context, catModel, _) {
          if (catModel.isLoading) {
            printLog('Loading');
            return Center(child: Container(child: kLoadingWidget(context)));
          }

          if (catModel.categories != null) {
            final categories = catModel.categories
                .where((item) => item.parent == '0')
                .toList();

            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  isDisplayDesktop(context)
                      ? SizedBox(
                          height: 100,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              const SizedBox(width: 20),
                              GestureDetector(
                                child: const Icon(Icons.arrow_back_ios,
                                    size: 22, color: Colors.white70),
                                onTap: () {
                                  if (isDisplayDesktop(context)) {
                                    eventBus
                                        .fire(const EventOpenCustomDrawer());
                                  }
                                  Navigator.of(context).pop();
                                },
                              ),
                              const SizedBox(width: 20),
                              Text(
                                S.of(context).products,
                                style: const TextStyle(
                                  fontFamily: 'DroidKuffi',
                                  fontSize: 21,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white70,
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox(),
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      S.of(context).layout.toUpperCase(),
                      style: const TextStyle(
                        fontFamily: 'DroidKuffi',
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Wrap(
                    children: <Widget>[
                      for (var item in kProductListLayout)
                        GestureDetector(
                          onTap: () =>
                              Provider.of<AppModel>(context, listen: false)
                                  .updateProductListLayout(item['layout']),
                          child: Container(
                            width: 40,
                            height: 40,
                            margin: const EdgeInsets.all(10.0),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                item['image'],
                                color: selectLayout == item['layout']
                                    ? Colors.white
                                    : Colors.black.withOpacity(0.2),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: selectLayout == item['layout']
                                    ? Colors.black.withOpacity(0.15)
                                    : Colors.black.withOpacity(0.05),
                                borderRadius: BorderRadius.circular(9.0)),
                          ),
                        )
                    ],
                  ),
                  if (!Config().isListingType()) ...[
                    const SizedBox(height: 40),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        S.of(context).byPrice.toUpperCase(),
                        style: const TextStyle(
                          fontFamily: 'DroidKuffi',
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          color: Colors.white70,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          Tools.getCurrencyFormatted(mixPrice, currencyRate,
                              currency: currency),
                          style: const TextStyle(
                              color: Colors.white, fontSize: 16),
                        ),
                        const Text(
                          " - ",
                          style: TextStyle(fontFamily:'DroidKuffi',color: Colors.white, fontSize: 16),
                        ),
                        Text(
                          Tools.getCurrencyFormatted(maxPrice, currencyRate,
                              currency: currency),
                          style: const TextStyle(
                            fontFamily: 'DroidKuffi',
                              color: Colors.white, fontSize: 16),
                        )
                      ],
                    ),
                    SliderTheme(
                      data: const SliderThemeData(
                        activeTrackColor: Color(kSliderActiveColor),
                        inactiveTrackColor: Color(kSliderInactiveColor),
                        activeTickMarkColor: Colors.white70,
                        inactiveTickMarkColor: Colors.black,
                        overlayColor: Colors.black12,
                        thumbColor: Color(kSliderActiveColor),
                        showValueIndicator: ShowValueIndicator.always,
                      ),
                      child: RangeSlider(
                        min: 0.0,
                        max: kMaxPriceFilter,
                        divisions: kFilterDivision,
                        values: RangeValues(mixPrice, maxPrice),
                        onChanged: (RangeValues values) {
                          setState(() {
                            mixPrice = values.start;
                            maxPrice = values.end;
                          });
                        },
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Padding(
                      padding: const EdgeInsets.only(left: 15, top: 30),
                      child: Text(
                        S.of(context).attributes.toUpperCase(),
                        style: const TextStyle(
                          fontFamily: 'DroidKuffi',
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          color: Colors.white70,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    ListenableProvider.value(
                      value: filterAttr,
                      child: Consumer<FilterAttributeModel>(
                        builder: (context, value, child) {
                          if (value.lstProductAttribute != null) {
                            List<Widget> list = List.generate(
                              value.lstProductAttribute.length,
                              (index) {
                                return FilterOptionItem(
                                  enabled: !value.isLoading,
                                  onTap: () {
                                    currentSelectedAttr = index;

                                    currentSlug =
                                        value.lstProductAttribute[index].slug;
                                    value.getAttr(
                                        id: value
                                            .lstProductAttribute[index].id);
                                  },
                                  title: value.lstProductAttribute[index].name
                                      .toUpperCase(),
                                  isValid: currentSelectedAttr != -1,
                                  selected: currentSelectedAttr == index,
                                );
                              },
                            );

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Container(
                                  child: Wrap(
                                    children: [
                                      ...list,
                                    ],
                                  ),
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                ),
                                value.isLoading
                                    ? Center(
                                        child: Container(
                                          margin: const EdgeInsets.only(
                                            top: 10.0,
                                          ),
                                          width: 25.0,
                                          height: 25.0,
                                          child:
                                              const CircularProgressIndicator(
                                                  strokeWidth: 2.0),
                                        ),
                                      )
                                    : Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5),
                                        child: currentSelectedAttr == -1
                                            ? Container()
                                            : Wrap(
                                                children: List.generate(
                                                  value.lstCurrentAttr.length,
                                                  (index) {
                                                    return Container(
                                                      margin: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 5),
                                                      child: FilterChip(
                                                        label: Text(
                                                            value
                                                            .lstCurrentAttr[
                                                                index]
                                                            .name,style: TextStyle(fontFamily: 'DroidKuffi'),),
                                                        selected: value
                                                                .lstCurrentSelectedTerms[
                                                            index],
                                                        onSelected: (val) {
                                                          value
                                                              .updateAttributeSelectedItem(
                                                                  index, val);
                                                        },
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                      ),
                              ],
                            );
                          }
                          return Container();
                        },
                      ),
                    ),
                  ],
                  Padding(
                    padding: const EdgeInsets.only(left: 15, top: 30),
                    child: Text(
                      S.of(context).byCategory.toUpperCase(),
                      style: const TextStyle(
                        fontFamily: 'DroidKuffi',
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 20),
                    child: Container(
                      padding: const EdgeInsets.only(top: 15.0),
                      decoration: BoxDecoration(
                          color: Colors.black12,
                          borderRadius: BorderRadius.circular(3.0)),
                      child: TreeView(
                        parentList: [
                          for (var item in categories)
                            Parent(
                              parent: CategoryItem(
                                item,
                                hasChild:
                                    hasChildren(catModel.categories, item.id),
                                isSelected: item.id == categoryId,
                                onTap: (category) => _onFilter(category, tagId),
                              ),
                              childList: ChildList(
                                children: [
                                  Parent(
                                    parent: CategoryItem(
                                      item,
                                      isLast: true,
                                      isParent: true,
                                      isSelected: item.id == categoryId,
                                      onTap: (category) =>
                                          _onFilter(category, tagId),
                                    ),
                                    childList: ChildList(
                                      children: const [],
                                    ),
                                  ),
                                  for (var category in getSubCategories(
                                      catModel.categories, item.id))
                                    Parent(
                                        parent: CategoryItem(
                                          category,
                                          isLast: true,
                                          isSelected: category.id == categoryId,
                                          onTap: (category) =>
                                              _onFilter(category, tagId),
                                        ),
                                        childList: ChildList(
                                          children: const [],
                                        ))
                                ],
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                ]
                  ..add(
                    ListenableProvider.value(
                      value: tag,
                      child: Consumer<TagModel>(
                        builder: (context, TagModel tagModel, _) {
                          if (tagModel.tagList.isEmpty) return const SizedBox();
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: tagModel.isLoading
                                ? [
                                    Center(
                                      child: Container(
                                        child: kLoadingWidget(context),
                                      ),
                                    )
                                  ]
                                : [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 15,
                                        top: 30,
                                      ),
                                      child: Text(
                                        S.of(context).byTag.toUpperCase(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white70,
                                          fontFamily: 'DroidKuffi'
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                        top: 15,
                                      ),
                                      child: Wrap(
                                        children: []..addAll(
                                            List.generate(
                                              tagModel.tagList?.length ?? 0,
                                              (index) {
                                                final bool selected = tagId ==
                                                    tagModel.tagList[index].id
                                                        .toString();
                                                return FilterOptionItem(
                                                  enabled: !tagModel.isLoading,
                                                  selected: selected,
                                                  isValid: tagId != '-1',
                                                  title: tagModel
                                                      .tagList[index].name
                                                      .toUpperCase(),
                                                  onTap: () {
                                                    setState(() {
                                                      if (selected) {
                                                        tagId = null;
                                                      } else {
                                                        tagId = tagModel
                                                            .tagList[index].id
                                                            .toString();
                                                      }
                                                    });
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                      ),
                                    ),
                                  ],
                          );
                        },
                      ),
                    ),
                  )
                  ..addAll([
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                        right: 15,
                        top: 30,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: ButtonTheme(
                              height: 44,
                              child: RaisedButton(
                                elevation: 0.0,
                                color: Colors.white70,
                                onPressed: () => _onFilter(categoryId, tagId),
                                child: Text(S.of(context).apply,style: TextStyle(fontFamily: 'DroidKuffi'),),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3.0),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 70),
                  ]),
              ),
            );
          }

          return const SizedBox();
        },
      ),
    );
  }

  bool hasChildren(categories, id) {
    return categories.where((o) => o.parent == id).toList().length > 0;
  }

  List<Category> getSubCategories(categories, id) {
    return categories.where((o) => o.parent == id).toList();
  }
}
