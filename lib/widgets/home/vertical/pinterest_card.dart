import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../models/index.dart' show CartModel, Product;
import '../../common/start_rating.dart';

class PinterestCard extends StatelessWidget {
  final Product item;
  final width;
  final marginRight;
  final kSize size;
  final bool isHero;
  final bool showCart;
  final bool showHeart;
  final bool showOnlyImage;

  PinterestCard({
    this.item,
    this.width,
    this.size = kSize.medium,
    this.isHero = false,
    this.showHeart = true,
    this.showCart = false,
    this.showOnlyImage = false,
    this.marginRight = 10.0,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final addProductToCart = Provider.of<CartModel>(context).addProductToCart;
    final currency = Provider.of<CartModel>(context).currency;
    final currencyRates = Provider.of<CartModel>(context).currencyRates;
    final isTablet = Tools.isTablet(MediaQuery.of(context));

    double titleFontSize = isTablet ? 24.0 : 14.0;
    double iconSize = isTablet ? 24.0 : 18.0;
    double starSize = isTablet ? 20.0 : 10.0;

    void onTapProduct() {
      if (item.imageFeature == '') return;

      Navigator.of(context).pushNamed(
        RouteList.productDetail,
        arguments: item,
      );
    }

    return GestureDetector(
      onTap: onTapProduct,
      child: Container(
        color: Theme.of(context).cardColor,
        margin: const EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            Tools.image(
              url: item.imageFeature,
              width: width,
              size: kSize.medium,
              isResize: true,
              fit: BoxFit.fill,
            ),
            if (showOnlyImage == null || !showOnlyImage)
              Container(
                width: width,
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(
                    top: 10, left: 8, right: 8, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(item.name,
                        style: TextStyle(
                          fontFamily:'DroidKuffi',
                          fontSize: titleFontSize,
                          fontWeight: FontWeight.w600,
                        ),
                        maxLines: 1),
                    const SizedBox(height: 6),
                    Text(Tools.getPriceProduct(item, currencyRates, currency),
                        style: TextStyle(fontFamily:'DroidKuffi',color: theme.accentColor)),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        if (kAdvanceConfig['EnableRating'])
                          Expanded(
                            child: SmoothStarRating(
                                allowHalfRating: true,
                                starCount: 5,
                                label: Text(
                                  '${item.ratingCount}',
                                  style: const TextStyle(fontFamily:'DroidKuffi',fontSize: 12),
                                ),
                                rating: item.averageRating ?? 0.0,
                                size: starSize,
                                color: theme.primaryColor,
                                borderColor: theme.primaryColor,
                                spacing: 0.0),
                          ),
                        if (showCart && !item.isEmptyProduct())
                          IconButton(
                              padding: const EdgeInsets.all(2.0),
                              icon:
                                  Icon(Icons.add_shopping_cart, size: iconSize),
                              onPressed: () {
                                addProductToCart(product: item);
                              }),
                      ],
                    )
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
