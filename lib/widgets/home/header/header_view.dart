import 'package:flutter/material.dart';

import '../../../common/constants/general.dart';
import '../../../generated/l10n.dart';

class HeaderView extends StatelessWidget {
  final String headerText;
  final VoidCallback callback;
  final bool showSeeAll;

  HeaderView({this.headerText, this.showSeeAll = false, Key key, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(
      width: screenSize.width,
      color: Theme.of(context).backgroundColor,
      child: Container(
        width: screenSize.width / (2 / (screenSize.height / screenSize.width)),
        margin: const EdgeInsets.only(top: 10.0),
        padding: const EdgeInsets.only(
            left: 17.0, top: 20.0, right: 15.0, bottom: 15.0),
        child: Row(
          textBaseline: TextBaseline.alphabetic,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          children: <Widget>[
            Expanded(
              child: Text(
                headerText ?? '',
                style: TextStyle(
                  fontFamily: 'DroidKuffi',
                    fontSize: kIsWeb ? 22 : 18.0,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).accentColor),
              ),
            ),
            if (showSeeAll)
              InkResponse(
                onTap: callback,
                child: Text(
                  S.of(context).seeAll,
                  style: TextStyle(
                    fontFamily: 'DroidKuffi',
                      fontSize: kIsWeb ? 18 : 14,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).primaryColor),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
