import 'package:flutter/material.dart';

import '../../common/constants/general.dart';
import '../../common/packages.dart' show StoryWidget;
import '../../common/tools.dart';
import '../../services/index.dart';
import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider_items.dart';
import 'category/category_icon_items.dart';
import 'category/category_image_items.dart';
import 'header/header_search.dart';
import 'header/header_text.dart';
import 'horizontal/blog_list_items.dart';
import 'horizontal/horizontal_list_items.dart';
import 'horizontal/instagram_items.dart';
import 'horizontal/simple_list.dart';
import 'horizontal/video/index.dart';
import 'product_list_layout.dart';

class DynamicLayout extends StatelessWidget {
  final config;
  final setting;

  DynamicLayout(this.config, this.setting);

  @override
  Widget build(BuildContext context) {
    switch (config["layout"]) {
      case 'header_text':
        if (kIsWeb) return Container();
        return HeaderText(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case 'header_search':
        if (kIsWeb) return Container();
        return HeaderSearch(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );
      case 'featuredVendors':
        return Services().widget.renderFeatureVendor(config);
      case "category":
        return (config['type'] == 'image')
            ? CategoryImages(
                config: config,
                key: config['key'] != null ? Key(config['key']) : null,
              )
            : CategoryIcons(
                config: config,
                key: config['key'] != null ? Key(config['key']) : null,
              );

      case "bannerAnimated":
        if (kIsWeb) return Container();
        return BannerAnimated(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "bannerImage":
        if (config['isSlider'] == true) {
          return BannerSliderItems(
              config: config,
              key: config['key'] != null ? Key(config['key']) : null);
        }
        return BannerGroupItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "largeCardHorizontalListItems":
        return LargeCardHorizontalListItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "simpleVerticalListItems":
        return SimpleVerticalProductList(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "instagram":
        return InstagramItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "blog":
        return BlogListItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "video":
        return VideoLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "story":
        return StoryWidget(
          config: config,
          onTapStoryText: (cfg) {
            Utils.onTapNavigateOptions(context: context, config: cfg);
          },
        );
      case "threeColumn":
      case "twoColumn":
      case "staggered":
        return ProductListLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      default:
        return const SizedBox();
    }
  }
}
