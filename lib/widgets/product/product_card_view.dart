import 'dart:math' as math;

import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart' show AppModel, RecentModel, CartModel, Product;
import '../common/start_rating.dart';
import "../layout/adaptive.dart";
import 'heart_button.dart';

class ProductCard extends StatelessWidget {
  final Product item;
  final width;
  final maxWidth;
  final marginRight;
  final kSize size;
  final bool isHero;
  final bool showCart;
  final bool showHeart;
  final height;
  final bool hideDetail;
  final offset;
  final tablet;
  final List<int> ratioProduct;

  ProductCard({
    this.item,
    this.width,
    this.maxWidth,
    this.size = kSize.medium,
    this.isHero = false,
    this.showHeart = false,
    this.showCart = false,
    this.height,
    this.offset,
    this.hideDetail = false,
    this.tablet,
    this.marginRight = 6.0,
    this.ratioProduct,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final addProductToCart =
        Provider.of<CartModel>(context, listen: false).addProductToCart;
    final currency = Provider.of<AppModel>(context, listen: false).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final smallestUnitRate = Provider.of<AppModel>(context).smallestUnitRate ?? 1;
    final isTablet = tablet ?? Tools.isTablet(MediaQuery.of(context));

    if (item == null) return Container();

    final price = item.price != null ? double.tryParse(item.price) : 0.0;
    double regularPrice = 0.0;

    // ignore: unrelated_type_equality_checks
    if (item.regularPrice != null &&
        item.regularPrice.isNotEmpty &&
        item.regularPrice != '0.0') {
      regularPrice = (double.tryParse(item.regularPrice.toString()));
    }

    double titleFontSize = isTablet ? 18.0 : 14.0;
    double priceFontSize = isTablet ? 16.0 : 14.0;
    double subPriceFontSize = isTablet ? 14.0 : 12.0;
    double ratingCountFontSize = isTablet ? 16.0 : 12.0;
    double iconSize = isTablet ? 20.0 : 18.0;
    double starSize = 10.0;

    final gauss = offset != null
        ? math.exp(-(math.pow(offset.abs() - 0.5, 2) / 0.08))
        : 0.0;
    bool isSale = (item.onSale ?? false) &&
        Tools.getPriceProductValue(item, currency, onSale: true) !=
            Tools.getPriceProductValue(item, currency, onSale: false);
    if (item.type == 'variable') {
      isSale = item.onSale ?? false;
    }
    if (hideDetail) {
      return _buildImageFeature(
        () => _onTapProduct(context),
      );
    }
    var priceProduct = Tools.getPriceProductValue(
      item,
      currency,
      onSale: true,
    );

    /// Sold by widget
    Widget _soldByStore = item.store != null && item.store.name != ""
        ? Padding(
            padding: const EdgeInsets.only(top: 2),
            child: Text(
              S.of(context).soldBy + " " + item.store.name,
              style: const TextStyle(fontSize: 11, fontWeight: FontWeight.w500),
            ),
          )
        : Container();

    /// product name
    Widget _productTitle = Text(
      item.name ?? '',
      style: TextStyle(
        fontSize: titleFontSize,
        fontWeight: FontWeight.w600,
      ),
      maxLines: 1,
    );
//    final smallestUnitRate = Provider.of<AppModel>(context).smallestUnitRate ?? 1;
//    totalPrice:
//    (totalPrice * smallestUnitRate).round().toStringAsFixed(0),
    /// Product Pricing
    Widget _productPricing = Wrap(
      crossAxisAlignment: WrapCrossAlignment.end,
      children: <Widget>[
        Text(
          item.type == 'grouped'
              ? '${S.of(context).from} ${Tools.getPriceProduct(item, currencyRate, currency, onSale: true)}'
              : priceProduct == '0.0'
                  ? S.of(context).loading
                  : Utils.isListingTheme()
                      ? Tools.getCurrencyFormatted(
                          item.price ?? item.regularPrice ?? '0', null)
                      : Tools.getPriceProduct(item, currencyRate, currency,
                          onSale: true),
          style: Theme.of(context).textTheme.headline6.copyWith(
                fontSize: priceFontSize,
                color: theme.accentColor,
              ),
        ),
        if (isSale) ...[
          const SizedBox(width: 5),
          Text(
            item.type == 'grouped'
                ? ''
                : Tools.getPriceProduct(item, currencyRate, currency,
                    onSale: false),
            style: Theme.of(context).textTheme.headline6.copyWith(
                  fontSize: subPriceFontSize,
                  fontWeight: FontWeight.w300,
                  color: Theme.of(context).accentColor.withOpacity(0.6),
                  decoration: TextDecoration.lineThrough,
                ),
          ),
        ]
      ],
    );

    /// Product Stock Status
    Widget _stockStatus = (kAdvanceConfig['showStockStatus'] &&
            !item.isEmptyProduct())
        ? item.backOrdered != null && item.backOrdered
            ? Text(
                '${S.of(context).backOrder}',
                style: const TextStyle(
                  color: kColorBackOrder,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              )
            : item.inStock != null
                ? Text(
                    item.inStock
                        ? S.of(context).inStock
                        : S.of(context).outOfStock,
                    style: TextStyle(
                      fontFamily: 'DroidKuffi',
                      color: item.inStock ? kColorInStock : kColorOutOfStock,
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  )
                : Container()
        : Container();

    /// product rating
    Widget _rating = (kAdvanceConfig['EnableRating']) &&
            (kAdvanceConfig['hideEmptyProductListRating'] == false ||
                (item.ratingCount != null && item.ratingCount > 0))
        ? SmoothStarRating(
            allowHalfRating: true,
            starCount: 5,
            rating: item.averageRating ?? 0.0,
            size: starSize,
            color: kColorRatingStar,
            borderColor: kColorRatingStar,
            label: Text(
              item.ratingCount == 0 || item.ratingCount == null
                  ? ''
                  : '${item.ratingCount}',
              style: TextStyle(
                fontSize: ratingCountFontSize,
              ),
            ),
            spacing: 0.0)
        : Container();

    /// Show Cart button
    Widget _showCart = (showCart &&
            !item.isEmptyProduct() &&
            item.inStock != null &&
            item.inStock &&
            item.type != "variable")
        ? IconButton(
            icon: Icon(Icons.add_shopping_cart, size: iconSize),
            onPressed: () {
              String message =
                  addProductToCart(product: item, context: context);
              _showFlashNotification(item, message, context);
            })
        : Container();

    /// Show Stock status & Rating
    Widget _productStockRating = Align(
      alignment: Alignment.bottomLeft,
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _stockStatus,
                _rating,
                const SizedBox(height: 4),
              ],
            ),
            const SizedBox(width: 10),
            _showCart,
          ],
        ),
      ),
    );

    Widget _productImage = Stack(
      children: <Widget>[
        Container(
          constraints: BoxConstraints(maxHeight: width * 1.2),
          child: Transform.translate(
            offset: Offset(18 * gauss, 0.0),
            child: _buildImageFeature(
              () => _onTapProduct(context),
            ),
          ),
        ),
        if (isSale &&
            (item.regularPrice?.isNotEmpty ?? false) &&
            regularPrice != null &&
            regularPrice != 0.0 &&
            item.type != 'variable')
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              decoration: const BoxDecoration(
                  color: Colors.redAccent,
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(8))),
              child: Text(
                '${(regularPrice - price) * 100 ~/ regularPrice} %',
                style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
        if (isSale && item.type == 'variable')
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              decoration: const BoxDecoration(
                  color: Colors.redAccent,
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(8))),
              child: Text(
                S.of(context).onSale,
                style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
      ],
    );

    Widget _productInfo = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 15),
        _productTitle,
        const SizedBox(height: 8),
        _productPricing,
        const SizedBox(height: 2),
        _productStockRating,
        _soldByStore,
      ],
    );

    /// Resize product base on ratio seting
    if (ratioProduct != null) {
      _productImage = Expanded(
        flex: ratioProduct[0],
        child: _productImage,
      );
      _productInfo = Expanded(
        flex: ratioProduct[1],
        child: _productInfo,
      );
    }

    return GestureDetector(
      onTap: () => _onTapProduct(context),
      behavior: HitTestBehavior.opaque,
      child: Stack(
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: maxWidth ?? width),
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 6.0),
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(3.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _productImage,
                _productInfo,
              ],
            ),
          ),
          if (showHeart && !item.isEmptyProduct())
            Positioned(
              top: 10,
              right: 10,
              child: HeartButton(product: item, size: 18),
            )
        ],
      ),
    );
  }

  Widget _buildImageFeature(onTapProduct) {
    // double _maxWidth = maxWidth ?? width;
    // double _height = math.min(_maxWidth * 1.2, height ?? width * 1.2);
    return GestureDetector(
      onTap: onTapProduct,
      child: Tools.image(
        url: item.imageFeature,
        width: width,
        size: kSize.medium,
        isResize: true,
        // height: _height,
        fit: BoxFit.cover,
        offset: offset ?? 0.0,
      ),
    );
  }

  void _onTapProduct(context) {
    if (item.imageFeature == '') return;
    Provider.of<RecentModel>(context, listen: false).addRecentProduct(item);
    //Load update product detail screen for FluxBuilder
    eventBus.fire('detail');
    Navigator.of(
      context,
      rootNavigator: !isBigScreen(context), // Push in tab for tablet (IPad)
    ).pushNamed(
      RouteList.productDetail,
      arguments: item,
    );
  }

  void _showFlashNotification(Product product, String message, context) {
    if (message.isNotEmpty) {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).errorColor,
            controller: controller,
            style: FlashStyle.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              message: Text(
                message,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          );
        },
      );
    } else {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).primaryColor,
            controller: controller,
            style: FlashStyle.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              title: Text(
                product.name,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 15.0,
                ),
              ),
              message: Text(
                S.of(context).addToCartSucessfully,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          );
        },
      );
    }
  }
}
