import 'package:flutter/material.dart';
import '../../common/config/general.dart';

import '../../common/tools.dart';
import '../../models/app_model.dart';
class StaticSplashScreen extends StatefulWidget {
  final String imagePath;
  final Widget onNextScreen;
  final int duration;
  final Key key;

  StaticSplashScreen({
    this.imagePath,
    this.key,
    this.onNextScreen,
    this.duration = 2500,
  });

  @override
  _StaticSplashScreenState createState() => _StaticSplashScreenState();
}

class _StaticSplashScreenState extends State<StaticSplashScreen>
     {

  void afterFirstLayout(BuildContext context) {
    AppModel.getCurrentValue().then((value) {
      print(value);
      print('ffffffffffffffffffffff');
      // LoginSMSConstants.irqValue = int.parse(value);
      // print('${LoginSMSConstants.irqValue}');
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => widget.onNextScreen));

    });
    // Future.delayed(Duration(milliseconds: widget.duration), () {
    //   Navigator.of(context).pushReplacement(
    //       MaterialPageRoute(builder: (context) => widget.onNextScreen));
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: widget.imagePath.startsWith('http')
            ? Tools.image(
                url: widget.imagePath,
                fit: BoxFit.contain,
              )
            : Image.asset(widget.imagePath,
                gaplessPlayback: true, fit: BoxFit.contain),
      ),
    );
  }
}
