/**
 * Everything Config about the Product Setting
 */

// TODO: 2-Update Product Variant Design Layouts
/// The product variant config
const ProductVariantLayout = {
  "color": "color",
  "size": "box",
  "height": "option",
};

// TODO: 2-Update Product Detail Layouts
/// use to config the product image height for the product detail
/// height=(percent * width-screen)
/// isHero: support hero animate
const kProductDetail = {
  "height": 0.4,
  "marginTop": 0,
  "isHero": false,
  "safeArea": false,
  "showVideo": true,
  "showThumbnailAtLeast": 1,
  "layout": "simpleType",
  "enableReview": false,
};

const kCartDetail = {
  "minAllowTotalCartValue": 0,
  "maxAllowQuantity": 10,
};

// TODO: 2-Update Product Variant Multi-Languages
const kProductVariantLanguage = {
  "en": {
    "color": "Color",
    "size": "Size",
    "height": "Height",
  },
  "ar": {"color": "اللون", "size": "بحجم", "height": "ارتفاع"},
  "vi": {
    "color": "Màu",
    "size": "Kích thước",
    "height": "Chiều Cao",
  },
};

// TODO: 2-Exclude The Category
/// Exclude this categories from the list
const kExcludedCategory = 311;
