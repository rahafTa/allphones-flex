import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// TODO: 4-Update SmartChat Features
const kConfigChat = {
  "EnableSmartChat": true,
};

/// config for the chat app
/// config Whatapp: https://faq.whatsapp.com/en/iphone/23559013
const smartChat = [
  {'app': 'https://wa.me/9647902633523', 'iconData': FontAwesomeIcons.whatsapp},
  {'app': 'tel:9647812223360', 'iconData': FontAwesomeIcons.phone},
  {'app': 'sms://9647812223360', 'iconData': FontAwesomeIcons.sms},
//  {'app': 'firebase', 'iconData': FontAwesomeIcons.google},
//  {
//    'app': 'https://tawk.to/chat/5e5cab81a89cda5a1888d472/default',
//    'iconData': FontAwesomeIcons.facebookMessenger
//  }
];
const String adminEmail = "info@allphones-iq.com";
