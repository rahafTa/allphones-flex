// TODO: 4-Update Welcome Screens
/// the welcome screen data
/// set onBoardingData = [] if you would like to hide the onboarding
List onBoardingData = [
  {
    "title": "مرحبا بك في متجر allphones",
    "image": "assets/images/fogg-delivery-1.png",
    "desc": "هو أفضل طريقة لحفظ مالك. "
  },
  {
    "title": "ابقَ على اتصال بعالمك المحيط",
    "image": "assets/images/fogg-uploading-1.png",
    "desc":
        "اطّلع على كل ما يحدث حولك فقط بضغطة من موبايلك سريع , مناسب ونظيف."
  },
  {
    "title": "لنبدأ",
    "image": "assets/images/fogg-order-completed.png",
    "desc": "ماذا تنتظر دعنا نبدأ!"
  },
];
