import '../../common/constants.dart';

/// Default app config, it's possible to set as URL
const kAppConfig = 'lib/config/config_ar.json';

// TODO: 4-Update Google Map Address
/// Ref: https://support.inspireui.com/help-center/articles/3/25/16/google-map-address
/// The Google API Key to support Pick up the Address automatically
/// We recommend to generate both ios and android to restrict by bundle app id
/// The download package is remove these keys, please use your own key
const kGoogleAPIKey = {
  "android": "AIzaSyDW3uXzZepWBPi-69BIYKyS-xo9NjFSFhQ",
  "ios": "AIzaSyDnBpxFOfeG6P06nK97hMg01kEgX48JhLE",
  "web": "AIzaSyDW3uXzZepWBPi-69BIYKyS-xo9NjFSFhQ"
};

/// user for upgrader version of app, remove the comment from lib/app.dart to enable this feature
/// https://tppr.me/5PLpD
const kUpgradeURLConfig = {
  "android":
      "https://play.google.com/store/apps/details?id=com.inspireui.fluxstore",
  "ios": "https://apps.apple.com/us/app/mstore-flutter/id1469772800"
};

/// use for rating app on store feature
const kStoreIdentifier = {
  "android": "com.inspireui.fluxstore",
  "ios": "1469772800"
};

   var kAdvanceConfig = {
  "DefaultLanguage": "ar",
  "DetailedBlogLayout": kBlogLayout.halfSizeImageType,
  "EnablePointReward": true,
  "hideOutOfStock": false,
  "EnableRating": true,
  "hideEmptyProductListRating": false,
  "EnableShipping": true,

  /// Enable search by SKU in search screen
  "EnableSkuSearch": true,

  /// Show stock Status on product List
  "showStockStatus": true,

  /// Gird count setting on Category screen
  "GridCount": 3,

  // TODO: 4.Upgrade App Performance & Image Optimize
  /// set isCaching to true if you have upload the config file to mstore-api
  /// set kIsResizeImage to true if you have finished running Re-generate image plugin
  /// ref: https://support.inspireui.com/help-center/articles/3/8/19/app-performance
  "isCaching": true,
  "kIsResizeImage": false,

  // TODO: 3.Update Mutli-Currencies and Default Currency
  /// Stripe payment only: set currencyCode and smallestUnitRate.
  /// All API requests expect amounts to be provided in a currency’s smallest unit.
  /// For example, to charge 10 USD, provide an amount value of 1000 (i.e., 1000 cents).
  /// Reference: https://stripe.com/docs/currencies#zero-decimal
  "DefaultCurrency": {
    "symbol": "\$",
    "decimalDigits": 2,
    "symbolBeforeTheNumber": true,
    "currency": "USD",
    "currencyCode": "usd",
    "smallestUnitRate": 100, // 100 cents = 1 usd
  },
  "Currencies": [
    {
      "symbol": "\$",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "USD",
      "currencyCode": "usd",
      "smallestUnitRate": 0 , // 100 cents = 1 usd
    },
    {
      "symbol": "د.ع",
      "decimalDigits": 3,
      "symbolBeforeTheNumber": true,
      "currency": "IQD",
      "currencyCode": "IQD",
      "smallestUnitRate": 1000, // 100 فلس  = 1 د.ع
    },





    /*{
      "symbol": "đ",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": false,
      "currency": "VND"
    },
    {
      "symbol": "€",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "Euro"
    },
    {
      "symbol": "£",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "Pound sterling",
      "currencyCode": "gbp",
      "smallestUnitRate": 100, // 100 pennies = 1 pound
    }*/
  ],

  // TODO: 3.Update Magento Config Product
  /// Below config is used for Magento store
  "DefaultStoreViewCode": "",
  "EnableAttributesConfigurableProduct": ["color", "size"],
  "EnableAttributesLabelConfigurableProduct": ["color", "size"],

  /// if the woo commerce website supports multi languages
  /// set false if the website only have one language
  "isMultiLanguages": true,

  ///Review gets approved automatically on woocommerce admin without requiring administrator to approve.
  "EnableApprovedReview": false,

  /// Sync Cart from website and mobile
  "EnableSyncCartFromWebsite": true,
  "EnableSyncCartToWebsite": true,
};

// TODO: 3.Update Social Login Login
/// ref: https://support.inspireui.com/help-center/articles/3/25/15/social-login
const kLoginSetting = {
  "IsRequiredLogin": false,
  'showAppleLogin': true,
  'showFacebook': true,
  'showSMSLogin': true,
  'showGoogleLogin': true,
};

// TODO: 3.Update Left Menu Setting
/// this could be config via Fluxbuilder tool http://fluxbuilder.com/
const kDefaultDrawer = {
  "logo": "assets/images/drawerLogo.png",
  "background": "",
  "items": [
    {"type": "home", "show": true},
    {"type": "blog", "show": true},
    {"type": "categories", "show": true},
    {"type": "cart", "show": true},
    {"type": "profile", "show": true},
    {"type": "login", "show": true},
    {"type": "rate", "show": true},
    {"type": "category", "show": true},
  ]
};

// TODO: 3.Update The Setting Screens Menu
/// you could order the position to change menu
/// this feature could be done via Fluxbuilder
const kDefaultSettings = [
  'products',
//  'chat',
//  'wishlist',
//  'notifications',
  'language',
  'currencies',
  'darkTheme',
  'order',
  'point',
//  'rating',
//  'privacy',
  'about',
  'account'
];

// TODO: 3.Update Push Notification For OneSignal
/// Ref: https://support.inspireui.com/help-center/articles/3/8/14/push-notifications
const kOneSignalKey = {'appID': '8b45b6db-7421-45e1-85aa-75e597f62714'};

/// Use for set default SMS Login
class LoginSMSConstants {
  static const String countryCodeDefault = 'IQ';
  static const String dialCodeDefault = '+964';
  static const String nameDefault = 'Iraq';
  // static  int irqValue = 0;
}

/// update default dark theme
/// advance color theme could be changed from common/styles.dart
const kDefaultDarkTheme = false;
