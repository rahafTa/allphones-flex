import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'common/constants/loading.dart';
import 'generated/l10n.dart';

class About extends StatelessWidget {
  final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'info@allphones-iq.com',
      queryParameters: {
        'subject': ''
      }
  );
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text(S.of(context).Contact_Info,style: TextStyle( color: Theme.of(context).accentColor,fontFamily: 'DroidKuffi',)),
      ),
      body: Center(
          child : Container(
              child:
              Padding(child: ListView(
                physics: BouncingScrollPhysics(),
                children: [
                  Image.asset(
                    'assets/images/logo.png',
                    width: 120,
                    height: 120,
                  ),
                  Center(child: Text(S.of(context).Allphones,
                      style: TextStyle(
                          fontSize: 28,
                          fontFamily: 'DroidKuffi',
                          color: Theme.of(context).accentColor)),),

                  Center(child: Text(S.of(context).Contact_Info
                      ,style: TextStyle(
                          fontSize: 24,
                          fontFamily: 'DroidKuffi',
                          color: Theme.of(context).primaryColor)
                  ),),

                  Column(
                    children: [
                      InkWell(child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                          title: Text('${S.of(context).Sales_number} 1',style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          subtitle: Text('+9647812223360',style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          leading: Icon(Icons.call),
                        ),
                      ),onTap: () async {
                        launch("tel:\+9647812223360");
                      },),

                      InkWell(child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                          title: Text('${S.of(context).Sales_number} 2',style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          subtitle: Text('+9647712223360',style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          leading: Icon(Icons.call),
                        ),
                      ),onTap: () async {
                        launch("tel:\+9647712223360");


                      },),

                      InkWell(child: Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: ListTile(
                          title: Text(S.of(context).Maintenance_number,style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          subtitle: Text('+9647732223366',style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          leading: Icon(Icons.call),
                        ),

                      ),onTap: () async {
                        launch("tel:\+9647732223366");
                      }),


                      InkWell(child:
                      Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                        title: Text(S.of(context).Call_office_WA,style: TextStyle(
                            fontFamily: 'DroidKuffi',
                            color: Theme.of(context).accentColor)),
                        subtitle: Text('+9647902633523',style: TextStyle(
                            fontFamily: 'DroidKuffi',
                            color: Theme.of(context).accentColor)),
                        leading: Icon(Icons.wifi_calling),
                      ),
                      ),
                          onTap: () async {
                            launch('https://wa.me/9647902633523');
                          }
                      ),


                      InkWell(child:
                      Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                        title: Text('info@allphones-iq.com',style: TextStyle(
                            fontFamily: 'DroidKuffi',
                            color: Theme.of(context).accentColor)),
                        leading: Icon(Icons.email),
                      ),
                      ),
                          onTap: ()  async{

                        launch(_emailLaunchUri.toString());
                        }

                      ),

                      InkWell(child:
                      Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                        title: Text(S.of(context).website,style: TextStyle(
                            fontFamily: 'DroidKuffi',
                            color: Theme.of(context).accentColor)),
                        leading: Icon(Icons.public),
                      ),),onTap: () async {
            launch("http://allphones-iq.com/");
          }),

                      InkWell(child:
                      Card(
                        elevation: 10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child:
                        ListTile(
                          title: Text(S.of(context).share,style: TextStyle(
                              fontFamily: 'DroidKuffi',
                              color: Theme.of(context).accentColor)),
                          leading: Icon(Icons.share),
                        ),),onTap: ()  {
                        Share.share('check out my website http://allphones-iq.com/');
                      })
                    ],
                  ),
                ],
              ),padding: EdgeInsets.all(20),)
              )),
    );
  }

}
