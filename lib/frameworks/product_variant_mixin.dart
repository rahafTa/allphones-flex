import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../generated/l10n.dart';
import '../models/index.dart'
    show Attribute, CartModel, Product, ProductModel, ProductVariation;
import '../screens/cart/cart.dart';
import '../widgets/common/webview.dart';
import '../widgets/product/product_variant.dart';

mixin ProductVariantMixin {
  ProductVariation updateVariation(
      List<ProductVariation> variations, Map<String, String> mapAttribute) {
    if (variations != null) {
      var variation = variations.firstWhere((item) {
        bool isCorrect = true;
        for (var attribute in item.attributes) {
          if (attribute.option != mapAttribute[attribute.name] &&
              (attribute.id != null ||
                  checkVariantLengths(variations, mapAttribute))) {
            isCorrect = false;
            break;
          }
        }
        if (isCorrect) {
          for (var key in mapAttribute.keys.toList()) {
            bool check = false;
            for (var attribute in item.attributes) {
              if (key == attribute.name) {
                check = true;
                break;
              }
            }
            if (!check) {
              Attribute att = Attribute()
                ..id = null
                ..name = key
                ..option = mapAttribute[key];
              item.attributes.add(att);
            }
          }
        }
        return isCorrect;
      }, orElse: () {
        return null;
      });
      return variation;
    }
    return null;
  }

  bool checkVariantLengths(variations, mapAttribute) {
    for (var variant in variations) {
      if (variant.attributes.length == mapAttribute.keys.toList().length) {
        bool check = true;
        for (var i = 0; i < variant.attributes.length; i++) {
          if (variant.attributes[i].option !=
              mapAttribute[variant.attributes[i].name]) {
            check = false;
            break;
          }
        }
        if (check) {
          return true;
        }
      }
    }
    return false;
  }

  bool isPurchased(
    ProductVariation productVariation,
    Product product,
    Map<String, String> mapAttribute,
    bool isAvailable,
  ) {
    bool inStock =
        productVariation != null ? productVariation.inStock : product.inStock;

    final isValidAttribute = product.attributes.length == mapAttribute.length &&
        (product.attributes.length == mapAttribute.length ||
            product.type != "variable");

    return inStock && isValidAttribute && isAvailable;
  }

  List<Widget> makeProductTitleWidget(BuildContext context,
      ProductVariation productVariation, Product product, bool isAvailable) {
    List<Widget> listWidget = [];

    bool inStock = (productVariation != null
            ? productVariation.inStock
            : product.inStock) ??
        false;

    String stockQuantity =
        product.stockQuantity != null ? ' (${product.stockQuantity}) ' : '';
    if (Provider.of<ProductModel>(context, listen: false).productVariation !=
        null) {
      stockQuantity = Provider.of<ProductModel>(context, listen: false)
                  .productVariation
                  .stockQuantity !=
              null
          ? ' (${Provider.of<ProductModel>(context, listen: false).productVariation.stockQuantity}) '
          : '';
    }

    if (isAvailable) {
      listWidget.add(
        const SizedBox(height: 5.0),
      );

      listWidget.add(
        Row(
          children: <Widget>[
            if (product.sku != null && product.sku != '') ...[
              Text(
                "${S.of(context).sku}: ",
                style: TextStyle(
                    fontSize: 15, color: Theme.of(context).accentColor),
              ),
              Text(
                product.sku,
                style: TextStyle(
                  color: inStock
                      ? Theme.of(context).primaryColor
                      : const Color(0xFFe74c3c),
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                ),
              ),
              const SizedBox(width: 8),
            ],
            Text(
              "${S.of(context).availability}: ",
              style:
                  TextStyle(fontFamily: 'DroidKuffi',fontSize: 15, color: Theme.of(context).accentColor),
            ),
            product.backOrdered != null && product.backOrdered
                ? Text(
                    '${S.of(context).backOrder}',
                    style: const TextStyle(
                      color: Color(0xFFEAA601),
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                    ),
                  )
                : Text(
                    inStock
                        ? '${S.of(context).inStock} ${stockQuantity ?? ''}'
                        : S.of(context).outOfStock,
                    style: TextStyle(
                      fontFamily: 'DroidKuffi',
                      color: inStock
                          ? Theme.of(context).primaryColor
                          : const Color(0xFFe74c3c),
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                    ),
                  )
          ],
        ),
      );

      listWidget.add(
        const SizedBox(height: 5.0),
      );
    }

    return listWidget;
  }

  List<Widget> makeBuyButtonWidget(
    BuildContext context,
    ProductVariation productVariation,
    Product product,
    Map<String, String> mapAttribute,
    int maxQuantity,
    int quantity,
    Function addToCart,
    Function onChangeQuantity,
    bool isAvailable,
  ) {
    final ThemeData theme = Theme.of(context);

    bool inStock = (productVariation != null
            ? productVariation.inStock
            : product.inStock) ??
        false;
    final isExternal = product.type == "external" ? true : false;

    if (!inStock && !isExternal) return [];

    return [
      if (!isExternal) const SizedBox(width: 10),
      if (!isExternal)
        Row(
          children: [
            Expanded(
              child: Text(
                S.of(context).selectTheQuantity + ":",
                style: TextStyle(
                  fontFamily: 'DroidKuffi',
                  fontSize: 15,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: 32.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                ),
                child: QuantitySelection(
                  expanded: true,
                  value: quantity,
                  color: theme.accentColor,
                  limitSelectQuantity: maxQuantity,
                  onChanged: onChangeQuantity,
                ),
              ),
            ),
          ],
        ),
      const SizedBox(height: 10),
      Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () => addToCart(true, inStock),
              child: Container(
                height: 44,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: isExternal
                      ? (inStock &&
                              (product.attributes.length ==
                                  mapAttribute.length) &&
                              isAvailable)
                          ? theme.primaryColor
                          : theme.disabledColor
                      : theme.primaryColor,
                ),
                child: Center(
                  child: Text(
                    ((inStock && isAvailable) || isExternal)
                        ? S.of(context).buyNow.toUpperCase()
                        : (isAvailable
                            ? S.of(context).outOfStock.toUpperCase()
                            : S.of(context).unavailable.toUpperCase()),
                    style: const TextStyle(
                      fontFamily: 'DroidKuffi',
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(width: 10),
          if (isAvailable && inStock && !isExternal)
            Expanded(
              child: GestureDetector(
                onTap: () => addToCart(false, inStock),
                child: Container(
                  height: 44,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: Theme.of(context).primaryColorLight,
                  ),
                  child: Center(
                    child: Text(
                      S.of(context).addToCart.toUpperCase(),
                      style: TextStyle(
                        fontFamily: 'DroidKuffi',
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ],
      )
    ];
  }

  /// Add to Cart & Buy Now function
  void addToCart(BuildContext context, Product product, int quantity,
      ProductVariation productVariation, Map<String, String> mapAttribute,
      [bool buyNow = false, bool inStock = false]) {
    if (!inStock) {
      return;
    }

    final cartModel = Provider.of<CartModel>(context, listen: false);
    if (product.type == "external") {
      openWebView(context, product);
      return;
    }

    final Map<String, String> _mapAttribute = Map.from(mapAttribute);
    productVariation =
        Provider.of<ProductModel>(context, listen: false).productVariation;
    String message = cartModel.addProductToCart(
        context: context,
        product: product,
        quantity: quantity,
        variation: productVariation,
        options: _mapAttribute);

    if (message.isNotEmpty) {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).errorColor,
            controller: controller,
            style: FlashStyle.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              message: Text(
                message,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          );
        },
      );
    } else {
      if (buyNow) {
        Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) => Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: CartScreen(isModal: true, isBuyNow: true),
            ),
            fullscreenDialog: true,
          ),
        );
      }
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).primaryColor,
            controller: controller,
            style: FlashStyle.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              title: Text(
                product.name,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 15.0,
                ),
              ),
              message: Text(
                S.of(context).addToCartSucessfully,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          );
        },
      );
    }
  }

  /// Support Affiliate product
  void openWebView(BuildContext context, Product product) {
    if (product.affiliateUrl == null || product.affiliateUrl.isEmpty) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(Icons.arrow_back_ios),
            ),
          ),
          body: const Center(
            child: Text("Not found"),
          ),
        );
      }));
      return;
    }

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebView(
          url: product.affiliateUrl,
          title: product.name,
        ),
      ),
    );
  }
}
