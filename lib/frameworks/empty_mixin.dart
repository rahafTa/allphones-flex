import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/config.dart';
import '../common/tools.dart';
import '../generated/l10n.dart';
import '../models/index.dart'
    show
        CartModel,
        OrderNote,
        AfterShip,
        UserModel,
        Order,
        Product,
        TaxModel,
        AppModel;
import '../screens/index.dart'
    show
        FullSizeLayout,
        HalfSizeLayout,
        ShippingMethods,
        SimpleLayout,
        SearchScreen;
import '../services/index.dart';
import '../widgets/common/webview.dart';

mixin EmptyMixin {
  /// Support Affiliate product
  void openWebView(BuildContext context, Product product) {
    if (product.affiliateUrl == null || product.affiliateUrl.isEmpty) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(Icons.arrow_back_ios),
            ),
          ),
          body: const Center(
            child: Text("Not found"),
          ),
        );
      }));
      return;
    }

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebView(
          url: product.affiliateUrl,
          title: product.name,
        ),
      ),
    );
  }

  Future<Product> getProductDetail(
          BuildContext context, Product product) async =>
      product;

  Widget renderCurrentPassInputforEditProfile(
          {BuildContext context,
          TextEditingController currentPasswordController}) =>
      Container();

  Future<AfterShip> getAllTracking() async => null;

  Future<List<OrderNote>> getOrderNote(
          {UserModel userModel, dynamic orderId}) async =>
      null;

  Future<Null> createReview(
      {String productId, Map<String, dynamic> data}) async {}

  Future<Null> getHomeCache(String lang) => null;

  Future<void> onLoadedAppConfig(String lang, Function callback) => null;

  Future<void> syncCartFromWebsite(String token, BuildContext context) async =>
      null;

  Future<void> syncCartToWebsite(CartModel cartModel) async => null;

  Widget renderTaxes(TaxModel taxModel, BuildContext context) => Container();

  void OnFinishOrder(BuildContext context, Function onSuccess, Order order) {
    onSuccess();
  }

  Widget renderVendorInfo(Product product) => Container();

  Widget renderVendorOrder(BuildContext context) => Container();

  Widget renderFeatureVendor(config) => Container();

  Widget renderShippingMethods(BuildContext context,
      {Function onBack, Function onNext}) {
    return ShippingMethods(onBack: onBack, onNext: onNext);
  }

  Widget renderVendorCategoriesScreen(data) => Container();

  Widget renderMapScreen() => Container();

  Future<void> resetPassword(BuildContext context, String username) => null;

  Product updateProductObject(Product product, Map json) => product;

  Future<Order> cancelOrder(BuildContext context, Order order) async => null;

  Widget renderButtons(Order order, cancelOrder, createRefund) => Container();

  Widget renderShippingMethodInfo(BuildContext context) {
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final model = Provider.of<CartModel>(context);

    return kPaymentConfig['EnableShipping']
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child:
                  Text(
                    '',
//                    S.of(context).pickupFromStore,
                    style: const TextStyle(fontFamily: 'DroidKuffi',fontSize: 16),
                  ),
                  /*Services().widget.renderShippingPaymentTitle(
                      context, "${model.shippingMethod.title}"),*/
                ),
                Text(
                  Tools.getCurrencyFormatted(
                      model.getShippingCost(), currencyRate,
                      currency: model.currency),
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                        fontSize: 14,
                        color: Theme.of(context).accentColor,
                      ),
                )
              ],
            ),
          )
        : Container();
  }

  Widget renderShippingPaymentTitle(BuildContext context, String title) {
    return Text(title,
        style: TextStyle(fontSize: 16, color: Theme.of(context).accentColor));
  }

  Widget renderRewardInfo(BuildContext context) {
    final currencyRate =
        Provider.of<AppModel>(context, listen: false).currencyRate;
    final currency = Provider.of<AppModel>(context, listen: false).currency;
    final rewardTotal =
        Provider.of<CartModel>(context, listen: false).rewardTotal;

    if (rewardTotal > 0) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              S.of(context).cartDiscount,
              style: TextStyle(
                fontSize: 14,
                color: Theme.of(context).accentColor,
              ),
            ),
            Text(
              Tools.getCurrencyFormatted(rewardTotal, currencyRate,
                  currency: currency),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    fontSize: 14,
                    color: Theme.of(context).accentColor,
                    fontWeight: FontWeight.w600,
                  ),
            )
          ],
        ),
      );
    }
    return Container();
  }

  Widget renderNewListing(context) => Container();

  Widget renderDetailScreen(context, product, layoutType) {
    switch (layoutType) {
      case 'halfSizeImageType':
        return HalfSizeLayout(product: product);
      case 'fullSizeImageType':
        return FullSizeLayout(product: product);
      default:
        return SimpleLayout(product: product);
    }
  }

  Widget renderSearchScreen(context, {showChat}) {
    return SearchScreen(
      key: const Key("search"),
      showChat: showChat,
    );
  }
}
